
-- depressive episode including remission (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F31.[3-5]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F32%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F33%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.75');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.76');

-- depressive episode including remission (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.2%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.3%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.5%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.82');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '311');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '298.0');

-- CPT4 depression (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107245);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107249);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40756912);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106304);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106305);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106310);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106322);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106326);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2101900);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40757095);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108573);

-- HCPCS depression (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617571);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617572);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617831);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617866);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617910);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533277);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533193);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533366);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533229);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786417);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786418);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786437);

-- manic episode including remission (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F30%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F31.[0-2]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.71');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.72');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.73');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.74');

-- manic episode including remission (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.0%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.1%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.4%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.81');

-- mixed episode including remission (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;mixed;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F31.6%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;mixed;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.77');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;mixed;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.78');

-- mixed episode including remission (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;mixed;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.6%');

-- unknown polarity episode including remission (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F31.8%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.7');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.70');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.9');

-- unknown polarity episode including remission (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.7');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.8');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.80');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.89');

-- no psychotic features (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F30.1%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F31.1%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F31.3%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F31.6[123]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.7');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.5');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33.42');

-- no psychotic features (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[0-6][1236]');

-- CPT4 Major depressive disorder, severe without psychotic features (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106310);

-- psychotic features (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.2');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.2');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.5');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.64');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.3');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33.3');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F23%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F24%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F28%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F29%');

-- psychotic features (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[0-6]4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.3');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '298.[013489]');

-- CPT4 psychotic depression (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106322);

-- unknown psychotic features (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.3');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.8');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.6');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.60');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F31.8%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.8');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.89');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F33.[8-9]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33.4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33.40');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33.41');

-- unknown psychotic features (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[0-6]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[0-6][05]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.7');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.8%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.9%');

-- mild episode (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.11');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.3');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.11');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.31');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.61');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F31.7%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.5');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F33.4%');

-- mild episode (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[0-6][156]');

-- CPT4 mild depression (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106304);

-- moderate episode (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.12');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.12');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.32');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.62');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.1');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33.1');

-- moderate episode (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[0-6]2');

-- CPT4 moderate depression (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106305);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107245);

-- severe episode (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.13');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.2');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.13');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.2');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.5');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.63');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.64');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F32.[2-3]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F33.[2-3]');

-- severe episode (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[0-6][3-4]');

-- CPT4 severe depression (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106310);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106322);

-- unknown severity episode (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.10');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F30.[89]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.10');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.30');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.60');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F31.8%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.8');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.89');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33.8');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33.9');

-- unknown severity episode (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[01234567]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.8%');

-- Congenital renal failure (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;congenital_failure;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'P96.0');

-- Hereditary nephropathy, not elsewhere classified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;hereditary_nephropathy;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N07');

-- Hereditary nephropathy, not elsewhere classified with minor glomerular abnormality (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;hereditary_nephropathy;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N07.0');

-- Hereditary nephropathy, not elsewhere classified with focal and segmental glomerular lesions (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;hereditary_nephropathy;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N07.1');

-- Hereditary nephropathy, not elsewhere classified with diffuse membranous glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;hereditary_nephropathy;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N07.2');

-- Hereditary nephropathy, not elsewhere classified with diffuse mesangial proliferative glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;hereditary_nephropathy;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N07.3');

-- Hereditary nephropathy, not elsewhere classified with diffuse endocapillary proliferative glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;hereditary_nephropathy;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N07.4');

-- Hereditary nephropathy, not elsewhere classified with diffuse mesangiocapillary glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;hereditary_nephropathy;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N07.5');

-- Hereditary nephropathy, not elsewhere classified with dense deposit disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;hereditary_nephropathy;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N07.6');

-- Hereditary nephropathy, not elsewhere classified with diffuse crescentic glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;hereditary_nephropathy;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N07.7');

-- Hereditary nephropathy, not elsewhere classified with other morphologic lesions (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;hereditary_nephropathy;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N07.8');

-- Hereditary nephropathy, not elsewhere classified with unspecified morphologic lesions (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;hereditary_nephropathy;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N07.9');

-- Acquired absence of kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_absence;acquired;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z90.5');

-- Acquired absence of kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_absence;acquired;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'V45.73');

-- Glomerular disorders in diseases classified elsewhere (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N08');

-- Diabetes mellitus due to underlying condition with diabetic nephropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.21');

-- Diabetes mellitus due to underlying condition with diabetic nephropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.21');

-- Diabetes mellitus due to underlying condition with diabetic chronic kidney disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.22');

-- Diabetes mellitus due to underlying condition with diabetic chronic kidney disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.22');

-- Diabetes mellitus due to underlying condition with other diabetic kidney complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.29');

-- Diabetes mellitus due to underlying condition with other diabetic kidney complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.29');

-- Drug or chemical induced diabetes mellitus with kidney complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.2');

-- Drug or chemical induced diabetes mellitus with diabetic nephropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.21');

-- Drug or chemical induced diabetes mellitus with diabetic nephropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.21');

-- Drug or chemical induced diabetes mellitus with diabetic nephropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.21');

-- Drug or chemical induced diabetes mellitus with diabetic chronic kidney disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.22');

-- Drug or chemical induced diabetes mellitus with diabetic chronic kidney disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.22');

-- Drug or chemical induced diabetes mellitus with diabetic chronic kidney disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.22');

-- Drug or chemical induced diabetes mellitus with other diabetic kidney complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.29');

-- Drug or chemical induced diabetes mellitus with other diabetic kidney complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.29');

-- Drug or chemical induced diabetes mellitus with other diabetic kidney complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.29');

-- Type 1 diabetes mellitus with kidney complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.2');

-- Type 1 diabetes mellitus with diabetic nephropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.21');

-- Type 1 diabetes mellitus with diabetic chronic kidney disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.22');

-- Type 1 diabetes mellitus with other diabetic kidney complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.29');

-- Type 2 diabetes mellitus with kidney complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.2');

-- Type 2 diabetes mellitus with diabetic nephropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.21');

-- Type 2 diabetes mellitus with diabetic chronic kidney disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.22');

-- Type 2 diabetes mellitus with other diabetic kidney complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.29');

-- Other specified diabetes mellitus with kidney complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.2');

-- Other specified diabetes mellitus with diabetic nephropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.21');

-- Other specified diabetes mellitus with diabetic chronic kidney disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.22');

-- Other specified diabetes mellitus with other diabetic kidney complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.29');

-- Secondary diabetes mellitus with renal manifestations (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.4');

-- Secondary diabetes mellitus with renal manifestations, not stated as uncontrolled, or unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.40');

-- Secondary diabetes mellitus with renal manifestations, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.41');

-- Diabetes with renal manifestations (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.4');

-- Diabetes with renal manifestations, type II or unspecified type, not stated as uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.40');

-- Diabetes with renal manifestations, type I [juvenile type], not stated as uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.41');

-- Diabetes with renal manifestations, type II or unspecified type, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.42');

-- Diabetes with renal manifestations, type I [juvenile type], uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.43');

-- Gouty nephropathy, unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '274.10');

-- Gouty nephropathy (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '274.1');

-- Other gouty nephropathy (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '274.19');

-- Hemorrhagic fever with renal syndrome (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;hemorrhagic_fever;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'A98.5');

-- Sicca (Sjogren) syndrome with tubulo-interstitial nephropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;sjogren;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M35.04');

-- Glomerular disease in systemic lupus erythematosus (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;sle;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M32.14');

-- Tubulo-interstitial nephropathy in systemic lupus erythematosus (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;sle;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M32.15');

-- Wegener's granulomatosis with renal involvement (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_disorder_secondary;wegener;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M31.31');

-- Kidney donor (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_donor;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z52.4');

-- Kidney donors (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_donor;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'V59.4');

-- Infection of kidney, unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '590.9');

-- Renal and perinephric abscess (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;abscess;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N15.1');

-- Renal and perinephric abscess (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;abscess;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '590.2');

-- Plasmodium malariae malaria with nephropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;diphtheria;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'B52.0');

-- Diphtheritic tubulo-interstitial nephropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;diphtheria;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'A36.84');

-- Gonococcal infection of kidney and ureter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;gonococ;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'A54.21');

-- Gonococcal infection (acute) of upper genitourinary tract, site unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;gonococ;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '098.10');

-- Gonococcal infection (acute) of upper genitourinary tract (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;gonococ;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '098.1');

-- Other gonococcal infection (acute) of upper genitourinary tract (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;gonococ;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '098.19');

-- Gonococcal infection, chronic, of upper genitourinary tract (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;gonococ;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '098.3');

-- Chronic gonococcal infection of upper genitourinary tract, site unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;gonococ;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '098.30');

-- Other chronic gonococcal infection of upper genitourinary tract (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;gonococ;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '098.39');

-- Sarcoid pyelonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;pyelonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'D86.84');

-- Pyelitis cystica (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;pyelonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N28.84');

-- Pyeloureteritis cystica (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;pyelonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N28.85');

-- Chronic pyelonephritis without lesion of renal medullary necrosis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;pyelonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '590.00');

-- Chronic pyelonephritis with lesion of renal medullary necrosis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;pyelonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '590.01');

-- Acute pyelonephritis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;pyelonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '590.1');

-- Acute pyelonephritis without lesion of renal medullary necrosis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;pyelonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '590.10');

-- Acute pyelonephritis with lesion of renal medullary necrosis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;pyelonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '590.11');

-- Pyeloureteritis cystica (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;pyelonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '590.3');

-- Other pyelonephritis or pyonephrosis, not specified as acute or chronic (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;pyelonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '590.8');

-- Pyelonephritis, unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;pyelonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '590.80');

-- Pyelitis or pyelonephritis in diseases classified elsewhere (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;pyelonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '590.81');

-- Chronic obstructive pyelonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;pyelonephritis;obstructive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N11.1');

-- Pyonephrosis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;pyonephrosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N13.6');

-- Salmonella pyelonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;salmonella;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'A02.25');

-- Syphilis of kidney and ureter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;syphilis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'A52.75');

-- Syphilis of kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;syphilis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '095.4');

-- Toxoplasma tubulo-interstitial nephropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;toxoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'B58.83');

-- Toxoplasma tubulo-interstitial nephropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;toxoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'B58.83');

-- Tuberculosis of kidney and ureter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;tuberculosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'A18.11');

-- Tuberculosis of kidney, unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;tuberculosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '016.00');

-- Tuberculosis of kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;tuberculosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '016.0');

-- Tuberculosis of kidney, bacteriological or histological examination not done (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;tuberculosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '016.01');

-- Tuberculosis of kidney, bacteriological or histological examination unknown (at present) (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;tuberculosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '016.02');

-- Tuberculosis of kidney, tubercle bacilli found (in sputum) by microscopy (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;tuberculosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '016.03');

-- Tuberculosis of kidney, tubercle bacilli not found (in sputum) by microscopy, but found by bacterial culture (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;tuberculosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '016.04');

-- Tuberculosis of kidney, tubercle bacilli not found by bacteriological examination, but tuberculosis confirmed histologically (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;tuberculosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '016.05');

-- Tuberculosis of kidney, tubercle bacilli not found by bacteriological or histological examination, but tuberculosis confirmed by other methods [inoculation of animals] (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_infection;tuberculosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '016.06');

-- Injury of kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.0');

-- Unspecified injury of kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.00');

-- Unspecified injury of right kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.001');

-- Unspecified injury of right kidney, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.001A');

-- Unspecified injury of right kidney, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.001D');

-- Unspecified injury of right kidney, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.001S');

-- Unspecified injury of left kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.002');

-- Unspecified injury of left kidney, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.002A');

-- Unspecified injury of left kidney, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.002D');

-- Unspecified injury of left kidney, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.002S');

-- Unspecified injury of unspecified kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.009');

-- Unspecified injury of unspecified kidney, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.009A');

-- Unspecified injury of unspecified kidney, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.009D');

-- Unspecified injury of unspecified kidney, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.009S');

-- Minor contusion of kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.01');

-- Minor contusion of right kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.011');

-- Minor contusion of right kidney, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.011A');

-- Minor contusion of right kidney, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.011D');

-- Minor contusion of right kidney, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.011S');

-- Minor contusion of left kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.012');

-- Minor contusion of left kidney, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.012A');

-- Minor contusion of left kidney, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.012D');

-- Minor contusion of left kidney, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.012S');

-- Minor contusion of unspecified kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.019');

-- Minor contusion of unspecified kidney, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.019A');

-- Minor contusion of unspecified kidney, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.019D');

-- Minor contusion of unspecified kidney, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.019S');

-- Major contusion of kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.02');

-- Major contusion of right kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.021');

-- Major contusion of right kidney, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.021A');

-- Major contusion of right kidney, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.021D');

-- Major contusion of right kidney, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.021S');

-- Major contusion of left kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.022');

-- Major contusion of left kidney, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.022A');

-- Major contusion of left kidney, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.022D');

-- Major contusion of left kidney, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.022S');

-- Major contusion of unspecified kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.029');

-- Major contusion of unspecified kidney, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.029A');

-- Major contusion of unspecified kidney, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.029D');

-- Major contusion of unspecified kidney, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.029S');

-- Laceration of kidney, unspecified degree (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.03');

-- Laceration of right kidney, unspecified degree (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.031');

-- Laceration of right kidney, unspecified degree, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.031A');

-- Laceration of right kidney, unspecified degree, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.031D');

-- Laceration of right kidney, unspecified degree, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.031S');

-- Laceration of left kidney, unspecified degree (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.032');

-- Laceration of left kidney, unspecified degree, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.032A');

-- Laceration of left kidney, unspecified degree, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.032D');

-- Laceration of left kidney, unspecified degree, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.032S');

-- Laceration of unspecified kidney, unspecified degree (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.039');

-- Laceration of unspecified kidney, unspecified degree, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.039A');

-- Laceration of unspecified kidney, unspecified degree, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.039D');

-- Laceration of unspecified kidney, unspecified degree, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.039S');

-- Minor laceration of kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.04');

-- Minor laceration of right kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.041');

-- Minor laceration of right kidney, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.041A');

-- Minor laceration of right kidney, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.041D');

-- Minor laceration of right kidney, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.041S');

-- Minor laceration of left kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.042');

-- Minor laceration of left kidney, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.042A');

-- Minor laceration of left kidney, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.042D');

-- Minor laceration of left kidney, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.042S');

-- Minor laceration of unspecified kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.049');

-- Minor laceration of unspecified kidney, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.049A');

-- Minor laceration of unspecified kidney, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.049D');

-- Minor laceration of unspecified kidney, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.049S');

-- Moderate laceration of kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.05');

-- Moderate laceration of right kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.051');

-- Moderate laceration of right kidney, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.051A');

-- Moderate laceration of right kidney, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.051D');

-- Moderate laceration of right kidney, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.051S');

-- Moderate laceration of left kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.052');

-- Moderate laceration of left kidney, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.052A');

-- Moderate laceration of left kidney, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.052D');

-- Moderate laceration of left kidney, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.052S');

-- Moderate laceration of unspecified kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.059');

-- Moderate laceration of unspecified kidney, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.059A');

-- Moderate laceration of unspecified kidney, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.059D');

-- Moderate laceration of unspecified kidney, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.059S');

-- Major laceration of kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.06');

-- Major laceration of right kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.061');

-- Major laceration of right kidney, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.061A');

-- Major laceration of right kidney, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.061D');

-- Major laceration of right kidney, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.061S');

-- Major laceration of left kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.062');

-- Major laceration of left kidney, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.062A');

-- Major laceration of left kidney, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.062D');

-- Major laceration of left kidney, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.062S');

-- Major laceration of unspecified kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.069');

-- Major laceration of unspecified kidney, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.069A');

-- Major laceration of unspecified kidney, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.069D');

-- Major laceration of unspecified kidney, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.069S');

-- Other injury of kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.09');

-- Other injury of right kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.091');

-- Other injury of right kidney, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.091A');

-- Other injury of right kidney, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.091D');

-- Other injury of right kidney, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.091S');

-- Other injury of left kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.092');

-- Other injury of left kidney, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.092A');

-- Other injury of left kidney, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.092D');

-- Other injury of left kidney, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.092S');

-- Other injury of unspecified kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.099');

-- Other injury of unspecified kidney, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.099A');

-- Other injury of unspecified kidney, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.099D');

-- Other injury of unspecified kidney, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S37.099S');

-- Injury of renal blood vessels (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.4');

-- Unspecified injury of renal blood vessel (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.40');

-- Unspecified injury of right renal artery (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.401');

-- Unspecified injury of right renal artery, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.401A');

-- Unspecified injury of right renal artery, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.401D');

-- Unspecified injury of right renal artery, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.401S');

-- Unspecified injury of left renal artery (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.402');

-- Unspecified injury of left renal artery, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.402A');

-- Unspecified injury of left renal artery, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.402D');

-- Unspecified injury of left renal artery, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.402S');

-- Unspecified injury of unspecified renal artery (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.403');

-- Unspecified injury of unspecified renal artery, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.403A');

-- Unspecified injury of unspecified renal artery, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.403D');

-- Unspecified injury of unspecified renal artery, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.403S');

-- Unspecified injury of right renal vein (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.404');

-- Unspecified injury of right renal vein, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.404A');

-- Unspecified injury of right renal vein, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.404D');

-- Unspecified injury of right renal vein, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.404S');

-- Unspecified injury of left renal vein (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.405');

-- Unspecified injury of left renal vein, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.405A');

-- Unspecified injury of left renal vein, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.405D');

-- Unspecified injury of left renal vein, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.405S');

-- Unspecified injury of unspecified renal vein (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.406');

-- Unspecified injury of unspecified renal vein, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.406A');

-- Unspecified injury of unspecified renal vein, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.406D');

-- Unspecified injury of unspecified renal vein, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.406S');

-- Laceration of renal blood vessel (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.41');

-- Laceration of right renal artery (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.411');

-- Laceration of right renal artery, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.411A');

-- Laceration of right renal artery, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.411D');

-- Laceration of right renal artery, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.411S');

-- Laceration of left renal artery (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.412');

-- Laceration of left renal artery, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.412A');

-- Laceration of left renal artery, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.412D');

-- Laceration of left renal artery, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.412S');

-- Laceration of unspecified renal artery (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.413');

-- Laceration of unspecified renal artery, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.413A');

-- Laceration of unspecified renal artery, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.413D');

-- Laceration of unspecified renal artery, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.413S');

-- Laceration of right renal vein (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.414');

-- Laceration of right renal vein, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.414A');

-- Laceration of right renal vein, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.414D');

-- Laceration of right renal vein, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.414S');

-- Laceration of left renal vein (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.415');

-- Laceration of left renal vein, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.415A');

-- Laceration of left renal vein, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.415D');

-- Laceration of left renal vein, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.415S');

-- Laceration of unspecified renal vein (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.416');

-- Laceration of unspecified renal vein, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.416A');

-- Laceration of unspecified renal vein, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.416D');

-- Laceration of unspecified renal vein, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.416S');

-- Other specified injury of renal blood vessel (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.49');

-- Other specified injury of right renal artery (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.491');

-- Other specified injury of right renal artery, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.491A');

-- Other specified injury of right renal artery, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.491D');

-- Other specified injury of right renal artery, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.491S');

-- Other specified injury of left renal artery (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.492');

-- Other specified injury of left renal artery, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.492A');

-- Other specified injury of left renal artery, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.492D');

-- Other specified injury of left renal artery, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.492S');

-- Other specified injury of unspecified renal artery (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.493');

-- Other specified injury of unspecified renal artery, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.493A');

-- Other specified injury of unspecified renal artery, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.493D');

-- Other specified injury of unspecified renal artery, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.493S');

-- Other specified injury of right renal vein (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.494');

-- Other specified injury of right renal vein, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.494A');

-- Other specified injury of right renal vein, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.494D');

-- Other specified injury of right renal vein, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.494S');

-- Other specified injury of left renal vein (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.495');

-- Other specified injury of left renal vein, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.495A');

-- Other specified injury of left renal vein, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.495D');

-- Other specified injury of left renal vein, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.495S');

-- Other specified injury of unspecified renal vein (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.496');

-- Other specified injury of unspecified renal vein, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.496A');

-- Other specified injury of unspecified renal vein, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.496D');

-- Other specified injury of unspecified renal vein, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S35.496S');

-- Injury to kidney without mention of open wound into cavity, unspecified injury (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '866.00');

-- Injury to kidney without mention of open wound into cavity (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '866.0');

-- Injury to kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '866');

-- Injury to kidney without mention of open wound into cavity, hematoma without rupture of capsule (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '866.01');

-- Injury to kidney without mention of open wound into cavity, laceration (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '866.02');

-- Injury to kidney without mention of open wound into cavity, complete disruption of kidney parenchyma (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '866.03');

-- Injury to kidney with open wound into cavity, unspecified injury (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '866.10');

-- Injury to kidney with open wound into cavity (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '866.1');

-- Injury to kidney with open wound into cavity, hematoma without rupture of capsule (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '866.11');

-- Injury to kidney with open wound into cavity, laceration (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '866.12');

-- Injury to kidney with open wound into cavity, complete disruption of kidney parenchyma (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '866.13');

-- Injury to renal blood vessels (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '902.4');

-- Injury to renal vessel(s), unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '902.40');

-- Injury to renal artery (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '902.41');

-- Injury to renal vein (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '902.42');

-- Injury to renal blood vessels, other (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '902.49');

-- Page kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N26.2');

-- Small kidney of unknown cause (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N27');

-- Small kidney, unilateral (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N27.0');

-- Small kidney, bilateral (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N27.1');

-- Small kidney, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N27.9');

-- Ischemia and infarction of kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N28.0');

-- Cyst of kidney, acquired (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N28.1');

-- Hypertrophy of kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N28.81');

-- Nephroptosis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N28.83');

-- Cystic kidney disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q61');

-- Polycystic kidney, infantile type (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q61.1');

-- Cystic dilatation of collecting ducts (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q61.11');

-- Other polycystic kidney, infantile type (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q61.19');

-- Polycystic kidney, adult type (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q61.2');

-- Polycystic kidney, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q61.3');

-- Renal dysplasia (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q61.4');

-- Medullary cystic kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q61.5');

-- Other cystic kidney diseases (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q61.8');

-- Cystic kidney disease, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q61.9');

-- Unilateral small kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '589.0');

-- Small kidney of unknown cause (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '589');

-- Bilateral small kidneys (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '589.1');

-- Small kidney, unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '589.9');

-- Cystic kidney disease (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '753.1');

-- Cystic kidney disease, unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '753.10');

-- Renal dysplasia (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '753.15');

-- Medullary cystic kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '753.16');

-- Medullary sponge kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '753.17');

-- Obstructive defects of renal pelvis and ureter, congenital (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '753.2');

-- Unspecified obstructive defect of renal pelvis and ureter (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '753.20');

-- Other obstructive defects of renal pelvis and ureter (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '753.29');

-- Other specified anomalies of kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '753.3');

-- Renal agenesis and other reduction defects of kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q60');

-- Renal agenesis, unilateral (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q60.0');

-- Renal agenesis, bilateral (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q60.1');

-- Renal agenesis, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q60.2');

-- Renal hypoplasia, unilateral (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q60.3');

-- Renal hypoplasia, bilateral (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q60.4');

-- Renal hypoplasia, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q60.5');

-- Potter's syndrome (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q60.6');

-- Congenital renal cyst (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q61.0');

-- Congenital renal cyst, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q61.00');

-- Congenital single renal cyst (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q61.01');

-- Congenital multiple renal cysts (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q61.02');

-- Congenital obstructive defects of renal pelvis and congenital malformations of ureter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q62');

-- Congenital hydronephrosis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q62.0');

-- Other congenital malformations of kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q63');

-- Accessory kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q63.0');

-- Lobulated, fused and horseshoe kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q63.1');

-- Ectopic kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q63.2');

-- Hyperplastic and giant kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q63.3');

-- Other specified congenital malformations of kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q63.8');

-- Congenital malformation of kidney, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q63.9');

-- Congenital vesico-uretero-renal reflux (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q62.7');

-- Renal agenesis and dysgenesis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '753.0');

-- Congenital single renal cyst (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '753.11');

-- Polycystic kidney, unspecified type (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '753.12');

-- Polycystic kidney, autosomal dominant (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '753.13');

-- Polycystic kidney, autosomal recessive (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '753.14');

-- Congenital obstruction of ureteropelvic junction (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;congenital;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '753.21');

-- Other and unspecified hydronephrosis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;hydronephrosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N13.3');

-- Unspecified hydronephrosis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;hydronephrosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N13.30');

-- Other hydronephrosis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;hydronephrosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N13.39');

-- Hydronephrosis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;hydronephrosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '591');

-- Other specified cystic kidney disease (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;kidney_structural_anomaly;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '753.19');

-- Benign lipomatous neoplasm of kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'D17.71');

-- Benign neoplasm of kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'D30.0');

-- Benign neoplasm of unspecified kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'D30.00');

-- Benign neoplasm of right kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'D30.01');

-- Benign neoplasm of left kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'D30.02');

-- Benign neoplasm of renal pelvis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'D30.1');

-- Benign neoplasm of unspecified renal pelvis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'D30.10');

-- Benign neoplasm of right renal pelvis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'D30.11');

-- Benign neoplasm of left renal pelvis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'D30.12');

-- Malignant neoplasm of kidney, except renal pelvis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'C64');

-- Malignant neoplasm of right kidney, except renal pelvis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'C64.1');

-- Malignant neoplasm of left kidney, except renal pelvis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'C64.2');

-- Malignant neoplasm of unspecified kidney, except renal pelvis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'C64.9');

-- Malignant neoplasm of renal pelvis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'C65');

-- Malignant neoplasm of right renal pelvis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'C65.1');

-- Malignant neoplasm of left renal pelvis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'C65.2');

-- Malignant neoplasm of unspecified renal pelvis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'C65.9');

-- Secondary malignant neoplasm of kidney and renal pelvis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'C79.0');

-- Secondary malignant neoplasm of unspecified kidney and renal pelvis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'C79.00');

-- Secondary malignant neoplasm of right kidney and renal pelvis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'C79.01');

-- Secondary malignant neoplasm of left kidney and renal pelvis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'C79.02');

-- Malignant carcinoid tumor of the kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'C7A.093');

-- Neoplasm of uncertain behavior of kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'D41.0');

-- Neoplasm of uncertain behavior of unspecified kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'D41.00');

-- Neoplasm of uncertain behavior of right kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'D41.01');

-- Neoplasm of uncertain behavior of left kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'D41.02');

-- Neoplasm of uncertain behavior of renal pelvis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'D41.1');

-- Neoplasm of uncertain behavior of unspecified renal pelvis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'D41.10');

-- Neoplasm of uncertain behavior of right renal pelvis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'D41.11');

-- Neoplasm of uncertain behavior of left renal pelvis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'D41.12');

-- Benign carcinoid tumor of the kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '209.64');

-- Benign neoplasm of kidney and other urinary organs (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '223');

-- Benign neoplasm of kidney, except pelvis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '223.0');

-- Malignant neoplasm of kidney, except pelvis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '189.0');

-- Malignant neoplasm of kidney and other and unspecified urinary organs (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '189');

-- Secondary malignant neoplasm of kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '198.0');

-- Malignant carcinoid tumor of the kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '209.24');

-- Malignant neoplasm of renal pelvis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '189.1');

-- Neoplasm of uncertain behavior of kidney and ureter (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '236.91');

-- Pregnancy related renal disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O26.83');

-- Pregnancy related renal disease, first trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O26.831');

-- Pregnancy related renal disease, first trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O26.831');

-- Pregnancy related renal disease, second trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O26.832');

-- Pregnancy related renal disease, second trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O26.832');

-- Pregnancy related renal disease, third trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O26.833');

-- Pregnancy related renal disease, third trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O26.833');

-- Pregnancy related renal disease, unspecified trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O26.839');

-- Pre-existing hypertensive chronic kidney disease complicating pregnancy, childbirth and the puerperium (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O10.2');

-- Pre-existing hypertensive chronic kidney disease complicating pregnancy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O10.21');

-- Pre-existing hypertensive chronic kidney disease complicating pregnancy, first trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O10.211');

-- Pre-existing hypertensive chronic kidney disease complicating pregnancy, first trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O10.211');

-- Pre-existing hypertensive chronic kidney disease complicating pregnancy, second trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O10.212');

-- Pre-existing hypertensive chronic kidney disease complicating pregnancy, second trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O10.212');

-- Pre-existing hypertensive chronic kidney disease complicating pregnancy, third trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O10.213');

-- Pre-existing hypertensive chronic kidney disease complicating pregnancy, third trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O10.213');

-- Pre-existing hypertensive chronic kidney disease complicating pregnancy, unspecified trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O10.219');

-- Pre-existing hypertensive chronic kidney disease complicating childbirth (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O10.22');

-- Pre-existing hypertensive chronic kidney disease complicating the puerperium (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O10.23');

-- Pre-existing hypertensive heart and chronic kidney disease complicating pregnancy, childbirth and the puerperium (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O10.3');

-- Pre-existing hypertensive heart and chronic kidney disease complicating pregnancy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O10.31');

-- Pre-existing hypertensive heart and chronic kidney disease complicating pregnancy, first trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O10.311');

-- Pre-existing hypertensive heart and chronic kidney disease complicating pregnancy, first trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O10.311');

-- Pre-existing hypertensive heart and chronic kidney disease complicating pregnancy, second trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O10.312');

-- Pre-existing hypertensive heart and chronic kidney disease complicating pregnancy, second trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O10.312');

-- Pre-existing hypertensive heart and chronic kidney disease complicating pregnancy, third trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O10.313');

-- Pre-existing hypertensive heart and chronic kidney disease complicating pregnancy, third trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O10.313');

-- Pre-existing hypertensive heart and chronic kidney disease complicating pregnancy, unspecified trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O10.319');

-- Pre-existing hypertensive heart and chronic kidney disease complicating childbirth (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O10.32');

-- Pre-existing hypertensive heart and chronic kidney disease complicating the puerperium (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O10.33');

-- Infections of kidney in pregnancy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O23.0');

-- Infections of kidney in pregnancy, unspecified trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O23.00');

-- Infections of kidney in pregnancy, first trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O23.01');

-- Infections of kidney in pregnancy, first trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O23.01');

-- Infections of kidney in pregnancy, second trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O23.02');

-- Infections of kidney in pregnancy, second trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O23.02');

-- Infections of kidney in pregnancy, third trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O23.03');

-- Infections of kidney in pregnancy, third trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O23.03');

-- Infection of kidney following delivery (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O86.21');

-- Postpartum acute kidney failure (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O90.4');

-- Kidney failure following abortion and ectopic and molar pregnancies (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '639.3');

-- Acute kidney failure following labor and delivery (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '669.3');

-- Acute kidney failure following labor and delivery, unspecified as to episode of care or not applicable (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '669.30');

-- Acute kidney failure following labor and delivery, delivered, with mention of postpartum complication (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '669.32');

-- Acute kidney failure following labor and delivery, postpartum condition or complication (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '669.34');

-- Spontaneous abortion, complicated by renal failure, unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '634.30');

-- Spontaneous abortion complicated by renal failure (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '634.3');

-- Spontaneous abortion, complicated by renal failure, incomplete (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '634.31');

-- Spontaneous abortion, complicated by renal failure, complete (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '634.32');

-- Legally induced abortion, complicated by renal failure,unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '635.30');

-- Legally induced abortion complicated by renal failure (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '635.3');

-- Legally induced abortion, complicated by renal failure, incomplete (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '635.31');

-- Legally induced abortion, complicated by renal failure, complete (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '635.32');

-- Illegally induced abortion, complicated by renal failure, unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '636.30');

-- Illegally induced abortion complicated by renal failure (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '636.3');

-- Illegally induced abortion, complicated by renal failure, incomplete (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '636.31');

-- Illegally induced abortion, complicated by renal failure, complete (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '636.32');

-- Unspecified abortion complicated by renal failure (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '637.3');

-- Unspecified abortion, complicated by renal failure, unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '637.30');

-- Unspecified abortion, complicated by renal failure, incomplete (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '637.31');

-- Unspecified abortion, complicated by renal failure, complete (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '637.32');

-- Failed attempted abortion complicated by renal failure (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '638.3');

-- Hypertension secondary to renal disease, complicating pregnancy, childbirth, and the puerperium, unspecified as to episode of care or not applicable (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '642.10');

-- Hypertension secondary to renal disease, complicating pregnancy, childbirth, and the puerperium (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '642.1');

-- Hypertension secondary to renal disease, complicating pregnancy, childbirth, and the puerperium, delivered, with or without mention of antepartum condition (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '642.11');

-- Hypertension secondary to renal disease, complicating pregnancy, childbirth, and the puerperium, delivered, with mention of postpartum complication (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '642.12');

-- Hypertension secondary to renal disease, complicating pregnancy, childbirth, and the puerperium, antepartum condition or complication (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '642.13');

-- Hypertension secondary to renal disease, complicating pregnancy, childbirth, and the puerperium, postpartum condition or complication (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '642.14');

-- Unspecified renal disease in pregnancy, without mention of hypertension (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '646.2');

-- Unspecified renal disease in pregnancy, without mention of hypertension, unspecified as to episode of care or not applicable (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '646.20');

-- Unspecified renal disease in pregnancy, without mention of hypertension, delivered, with or without mention of antepartum condition (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '646.21');

-- Unspecified renal disease in pregnancy, without mention of hypertension, delivered, with mention of postpartum complication (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '646.22');

-- Unspecified renal disease in pregnancy, without mention of hypertension, antepartum condition or complication (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '646.23');

-- Unspecified renal disease in pregnancy, without mention of hypertension, postpartum condition or complication (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '646.24');

-- Postprocedural (acute) (chronic) kidney failure (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;postprocedural;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N99.0');

-- Maternal renal and urinary tract diseases affecting fetus or newborn (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;kidney;renal_desease_newborn;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '760.1');

-- Autism-spectrum disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;autism_spectrum;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F84%');

-- Autism-spectrum disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;autism_spectrum;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '299%');

-- Delusional disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;delusion_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F22%');

-- Delusional disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;delusion_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;delusion_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.1');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;delusion_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.2');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;delusion_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.8');

-- Intellectual disabilities (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F70%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F71%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F72%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F73%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F78%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F79%');

-- Intellectual disabilities (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '317%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '318%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '319%');

-- Organic disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F01%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F02%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F03%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F04%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F06%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F07%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F09%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F48.2');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'G30%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F10.2[67]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F10.9[67]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F13.2[67]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F13.9[67]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F19.9[67]');

-- Organic disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '290%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '294%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '293.8%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '293.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '310%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '291.[12]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '292.8[23]');

-- Parkinson's disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;parkinson_disease;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'G20');

-- Parkinson's disease (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;parkinson_disease;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '332.0');

-- schizoaffective disorder (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;schizoaffective;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F25%');

-- schizoaffective disorder (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;schizoaffective;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '295.7%');

-- Schizophrenia (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;schizophrenia;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F20%');

-- Schizophrenia (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;exclusion;mental;schizophrenia;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '295.[012345689]%');

-- bipolar disorder (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;inclusion;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F30%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;inclusion;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F31%');

-- bipolar disorder (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;inclusion;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.0%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;inclusion;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.1%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;inclusion;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.4%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;inclusion;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.5%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;inclusion;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.6%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;inclusion;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.7');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;inclusion;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.8%');

-- Recurrent and persistent hematuria (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N02');

-- Recurrent and persistent hematuria with minor glomerular abnormality (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N02.0');

-- Recurrent and persistent hematuria with focal and segmental glomerular lesions (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N02.1');

-- Recurrent and persistent hematuria with diffuse membranous glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N02.2');

-- Recurrent and persistent hematuria with diffuse mesangial proliferative glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N02.3');

-- Recurrent and persistent hematuria with diffuse endocapillary proliferative glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N02.4');

-- Recurrent and persistent hematuria with diffuse mesangiocapillary glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N02.5');

-- Recurrent and persistent hematuria with dense deposit disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N02.6');

-- Recurrent and persistent hematuria with other morphologic changes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N02.8');

-- Recurrent and persistent hematuria with unspecified morphologic changes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N02.9');

-- Other disorders of kidney and ureter, not elsewhere classified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N28');

-- Ischemia and infarction of kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N28.0');

-- Other specified disorders of kidney and ureter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N28.8');

-- Other specified disorders of kidney and ureter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N28.89');

-- Disorder of kidney and ureter, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N28.9');

-- Other disorders of kidney and ureter in diseases classified elsewhere (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N29');

-- Acute glomerulonephritis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;glomerulonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '580');

-- Acute glomerulonephritis with lesion of proliferative glomerulonephritis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;glomerulonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '580.0');

-- Acute glomerulonephritis with other specified pathological lesion in kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;glomerulonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '580.8');

-- Acute glomerulonephritis in diseases classified elsewhere (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;glomerulonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '580.81');

-- Acute glomerulonephritis with other specified pathological lesion in kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;glomerulonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '580.89');

-- Acute glomerulonephritis with unspecified pathological lesion in kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;glomerulonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '580.9');

-- Acute nephritic syndrome (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N00');

-- Acute nephritic syndrome with minor glomerular abnormality (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N00.0');

-- Acute nephritic syndrome with focal and segmental glomerular lesions (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N00.1');

-- Acute nephritic syndrome with diffuse membranous glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N00.2');

-- Acute nephritic syndrome with diffuse mesangial proliferative glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N00.3');

-- Acute nephritic syndrome with diffuse endocapillary proliferative glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N00.4');

-- Acute nephritic syndrome with diffuse mesangiocapillary glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N00.5');

-- Acute nephritic syndrome with dense deposit disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N00.6');

-- Acute nephritic syndrome with other morphologic changes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N00.8');

-- Acute nephritic syndrome with unspecified morphologic changes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N00.9');

-- Chronic nephritic syndrome (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N03');

-- Chronic nephritic syndrome with minor glomerular abnormality (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N03.0');

-- Chronic nephritic syndrome with focal and segmental glomerular lesions (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N03.1');

-- Chronic nephritic syndrome with diffuse membranous glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N03.2');

-- Chronic nephritic syndrome with diffuse mesangial proliferative glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N03.3');

-- Chronic nephritic syndrome with diffuse endocapillary proliferative glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N03.4');

-- Chronic nephritic syndrome with diffuse mesangiocapillary glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N03.5');

-- Chronic nephritic syndrome with dense deposit disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N03.6');

-- Chronic nephritic syndrome with other morphologic changes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N03.8');

-- Chronic nephritic syndrome with unspecified morphologic changes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N03.9');

-- Unspecified nephritic syndrome (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N05');

-- Unspecified nephritic syndrome with minor glomerular abnormality (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N05.0');

-- Unspecified nephritic syndrome with focal and segmental glomerular lesions (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N05.1');

-- Unspecified nephritic syndrome with diffuse membranous glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N05.2');

-- Unspecified nephritic syndrome with diffuse mesangial proliferative glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N05.3');

-- Unspecified nephritic syndrome with diffuse endocapillary proliferative glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N05.4');

-- Unspecified nephritic syndrome with diffuse mesangiocapillary glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N05.5');

-- Unspecified nephritic syndrome with dense deposit disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N05.6');

-- Unspecified nephritic syndrome with other morphologic changes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N05.8');

-- Unspecified nephritic syndrome with unspecified morphologic changes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N05.9');

-- Acute tubulo-interstitial nephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N10');

-- Chronic tubulo-interstitial nephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N11');

-- Other chronic tubulo-interstitial nephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N11.8');

-- Chronic tubulo-interstitial nephritis, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N11.9');

-- Tubulo-interstitial nephritis, not specified as acute or chronic (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;acute;nephritic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N12');

-- Anemia in chronic kidney disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;anemia;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'D63.1');

-- Anemia in chronic kidney disease (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;anemia;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '285.21');

-- Anuria and oliguria (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;anuria_oliguria;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'R34');

-- Oliguria and anuria (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;anuria_oliguria;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '788.5');

-- Hydronephrosis with renal and ureteral calculous obstruction (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;calculus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N13.2');

-- Calculus of kidney and ureter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;calculus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N20');

-- Calculus of kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;calculus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N20.0');

-- Calculus of kidney with calculus of ureter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;calculus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N20.2');

-- Urinary calculus, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;calculus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N20.9');

-- Hydronephrosis with renal and ureteral calculous obstruction (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;calculus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N13.2');

-- Uric acid nephrolithiasis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;calculus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '274.11');

-- Calculus of kidney and ureter (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;calculus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '592');

-- Calculus of kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;calculus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '592.0');

-- Uric acid nephrolithiasis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;calculus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '274.11');

-- Chronic glomerulonephritis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;chronic;glomerulonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '582');

-- Chronic glomerulonephritis with lesion of proliferative glomerulonephritis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;chronic;glomerulonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '582.0');

-- Chronic glomerulonephritis with lesion of membranous glomerulonephritis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;chronic;glomerulonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '582.1');

-- Chronic glomerulonephritis with lesion of membranoproliferative glomerulonephritis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;chronic;glomerulonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '582.2');

-- Chronic glomerulonephritis with other specified pathological lesion in kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;chronic;glomerulonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '582.8');

-- Chronic glomerulonephritis in diseases classified elsewhere (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;chronic;glomerulonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '582.81');

-- Chronic glomerulonephritis with other specified pathological lesion in kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;chronic;glomerulonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '582.89');

-- Chronic glomerulonephritis with unspecified pathological lesion in kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;chronic;glomerulonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '582.9');

-- Chronic kidney disease, stage 1 (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;chronic;stage_1;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N18.1');

-- Chronic kidney disease, Stage I (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;chronic;stage_1;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '585.1');

-- Chronic kidney disease, stage 2 (mild) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;chronic;stage_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N18.2');

-- Chronic kidney disease, Stage II (mild) (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;chronic;stage_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '585.2');

-- Chronic kidney disease, stage 3 (moderate) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;chronic;stage_3;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N18.3');

-- Chronic kidney disease, Stage III (moderate) (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;chronic;stage_3;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '585.3');

-- Unspecified renal colic (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;colic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N23');

-- Nephrogenic diabetes insipidus (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;diabetes_insipidus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N25.1');

-- Nephrogenic diabetes insipidus (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;diabetes_insipidus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '588.1');

-- Nephritis and nephropathy, not specified as acute or chronic (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '583');

-- Nephritis and nephropathy, not specified as acute or chronic, with lesion of proliferative glomerulonephritis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '583.0');

-- Nephritis and nephropathy, not specified as acute or chronic, with lesion of membranous glomerulonephritis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '583.1');

-- Nephritis and nephropathy, not specified as acute or chronic, with lesion of membranoproliferative glomerulonephritis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '583.2');

-- Nephritis and nephropathy, not specified as acute or chronic, with other specified pathological lesion in kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '583.8');

-- Nephritis and nephropathy, not specified as acute or chronic, in diseases classified elsewhere (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '583.81');

-- Nephritis and nephropathy, not specified as acute or chronic, with other specified pathological lesion in kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '583.89');

-- Nephritis and nephropathy, not specified as acute or chronic, with unspecified pathological lesion in kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '583.9');

-- Nephrotic syndrome (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephrotic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N04');

-- Nephrotic syndrome with minor glomerular abnormality (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephrotic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N04.0');

-- Nephrotic syndrome with focal and segmental glomerular lesions (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephrotic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N04.1');

-- Nephrotic syndrome with diffuse membranous glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephrotic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N04.2');

-- Nephrotic syndrome with diffuse mesangial proliferative glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephrotic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N04.3');

-- Nephrotic syndrome with diffuse endocapillary proliferative glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephrotic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N04.4');

-- Nephrotic syndrome with diffuse mesangiocapillary glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephrotic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N04.5');

-- Nephrotic syndrome with dense deposit disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephrotic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N04.6');

-- Nephrotic syndrome with other morphologic changes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephrotic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N04.8');

-- Nephrotic syndrome with unspecified morphologic changes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephrotic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N04.9');

-- Nephrotic syndrome (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephrotic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '581');

-- Nephrotic syndrome with lesion of proliferative glomerulonephritis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephrotic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '581.0');

-- Nephrotic syndrome with lesion of membranous glomerulonephritis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephrotic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '581.1');

-- Nephrotic syndrome with lesion of membranoproliferative glomerulonephritis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephrotic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '581.2');

-- Nephrotic syndrome with lesion of minimal change glomerulonephritis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephrotic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '581.3');

-- Nephrotic syndrome with other specified pathological lesion in kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephrotic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '581.8');

-- Nephrotic syndrome in diseases classified elsewhere (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephrotic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '581.81');

-- Nephrotic syndrome with other specified pathological lesion in kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephrotic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '581.89');

-- Nephrotic syndrome with unspecified pathological lesion in kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;nephrotic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '581.9');

-- Isolated proteinuria with specified morphological lesion (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;proteinuria;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N06');

-- Isolated proteinuria with minor glomerular abnormality (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;proteinuria;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N06.0');

-- Isolated proteinuria with focal and segmental glomerular lesions (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;proteinuria;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N06.1');

-- Isolated proteinuria with diffuse membranous glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;proteinuria;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N06.2');

-- Isolated proteinuria with diffuse mesangial proliferative glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;proteinuria;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N06.3');

-- Isolated proteinuria with diffuse endocapillary proliferative glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;proteinuria;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N06.4');

-- Isolated proteinuria with diffuse mesangiocapillary glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;proteinuria;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N06.5');

-- Isolated proteinuria with dense deposit disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;proteinuria;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N06.6');

-- Isolated proteinuria with other morphologic lesion (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;proteinuria;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N06.8');

-- Isolated proteinuria with unspecified morphologic lesion (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;proteinuria;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N06.9');

-- Proteinuria (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;proteinuria;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'R80');

-- Isolated proteinuria (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;proteinuria;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'R80.0');

-- Persistent proteinuria, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;proteinuria;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'R80.1');

-- Bence Jones proteinuria (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;proteinuria;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'R80.3');

-- Other proteinuria (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;proteinuria;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'R80.8');

-- Proteinuria, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;proteinuria;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'R80.9');

-- Proteinuria (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;proteinuria;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '791.0');

-- Disorders resulting from impaired renal tubular function (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N25');

-- Other disorders resulting from impaired renal tubular function (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N25.8');

-- Other disorders resulting from impaired renal tubular function (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N25.89');

-- Disorder resulting from impaired renal tubular function, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N25.9');

-- Disorders resulting from impaired renal function (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '588');

-- Other specified disorders resulting from impaired renal function (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '588.8');

-- Other specified disorders resulting from impaired renal function (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '588.89');

-- Unspecified disorder resulting from impaired renal function (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '588.9');

-- Gout due to renal impairment (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.3');

-- Gout due to renal impairment, unspecified site (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.30');

-- Gout due to renal impairment, right shoulder (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.311');

-- Gout due to renal impairment, left shoulder (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.312');

-- Gout due to renal impairment, unspecified shoulder (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.319');

-- Gout due to renal impairment, elbow (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.32');

-- Gout due to renal impairment, right elbow (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.321');

-- Gout due to renal impairment, left elbow (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.322');

-- Gout due to renal impairment, unspecified elbow (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.329');

-- Gout due to renal impairment, wrist (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.33');

-- Gout due to renal impairment, right wrist (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.331');

-- Gout due to renal impairment, left wrist (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.332');

-- Gout due to renal impairment, unspecified wrist (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.339');

-- Gout due to renal impairment, hand (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.34');

-- Gout due to renal impairment, right hand (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.341');

-- Gout due to renal impairment, left hand (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.342');

-- Gout due to renal impairment, unspecified hand (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.349');

-- Gout due to renal impairment, hip (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.35');

-- Gout due to renal impairment, right hip (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.351');

-- Gout due to renal impairment, left hip (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.352');

-- Gout due to renal impairment, unspecified hip (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.359');

-- Gout due to renal impairment, knee (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.36');

-- Gout due to renal impairment, right knee (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.361');

-- Gout due to renal impairment, left knee (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.362');

-- Gout due to renal impairment, unspecified knee (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.369');

-- Gout due to renal impairment, ankle and foot (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.37');

-- Gout due to renal impairment, right ankle and foot (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.371');

-- Gout due to renal impairment, left ankle and foot (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.372');

-- Gout due to renal impairment, unspecified ankle and foot (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.379');

-- Gout due to renal impairment, vertebrae (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.38');

-- Gout due to renal impairment, multiple sites (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M10.39');

-- Chronic gout due to renal impairment (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3');

-- Chronic gout due to renal impairment, unspecified site (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.30');

-- Chronic gout due to renal impairment, unspecified site, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.30X0');

-- Chronic gout due to renal impairment, unspecified site, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.30X1');

-- Chronic gout due to renal impairment, unspecified site, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.30X1');

-- Chronic gout due to renal impairment, shoulder (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.31');

-- Chronic gout due to renal impairment, right shoulder (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.311');

-- Chronic gout due to renal impairment, right shoulder, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3110');

-- Chronic gout due to renal impairment, right shoulder, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3111');

-- Chronic gout due to renal impairment, right shoulder, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3111');

-- Chronic gout due to renal impairment, left shoulder (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.312');

-- Chronic gout due to renal impairment, left shoulder, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3120');

-- Chronic gout due to renal impairment, left shoulder, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3121');

-- Chronic gout due to renal impairment, left shoulder, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3121');

-- Chronic gout due to renal impairment, unspecified shoulder (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.319');

-- Chronic gout due to renal impairment, unspecified shoulder, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3190');

-- Chronic gout due to renal impairment, unspecified shoulder, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3191');

-- Chronic gout due to renal impairment, unspecified shoulder, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3191');

-- Chronic gout due to renal impairment, elbow (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.32');

-- Chronic gout due to renal impairment, right elbow (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.321');

-- Chronic gout due to renal impairment, right elbow, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3210');

-- Chronic gout due to renal impairment, right elbow, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3211');

-- Chronic gout due to renal impairment, right elbow, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3211');

-- Chronic gout due to renal impairment, left elbow (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.322');

-- Chronic gout due to renal impairment, left elbow, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3220');

-- Chronic gout due to renal impairment, left elbow, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3221');

-- Chronic gout due to renal impairment, left elbow, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3221');

-- Chronic gout due to renal impairment, unspecified elbow (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.329');

-- Chronic gout due to renal impairment, unspecified elbow, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3290');

-- Chronic gout due to renal impairment, unspecified elbow, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3291');

-- Chronic gout due to renal impairment, unspecified elbow, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3291');

-- Chronic gout due to renal impairment, wrist (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.33');

-- Chronic gout due to renal impairment, right wrist (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.331');

-- Chronic gout due to renal impairment, right wrist, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3310');

-- Chronic gout due to renal impairment, right wrist, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3311');

-- Chronic gout due to renal impairment, right wrist, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3311');

-- Chronic gout due to renal impairment, left wrist (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.332');

-- Chronic gout due to renal impairment, left wrist, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3320');

-- Chronic gout due to renal impairment, left wrist, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3321');

-- Chronic gout due to renal impairment, left wrist, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3321');

-- Chronic gout due to renal impairment, unspecified wrist (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.339');

-- Chronic gout due to renal impairment, unspecified wrist, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3390');

-- Chronic gout due to renal impairment, unspecified wrist, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3391');

-- Chronic gout due to renal impairment, unspecified wrist, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3391');

-- Chronic gout due to renal impairment, hand (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.34');

-- Chronic gout due to renal impairment, right hand (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.341');

-- Chronic gout due to renal impairment, right hand, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3410');

-- Chronic gout due to renal impairment, right hand, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3411');

-- Chronic gout due to renal impairment, right hand, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3411');

-- Chronic gout due to renal impairment, left hand (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.342');

-- Chronic gout due to renal impairment, left hand, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3420');

-- Chronic gout due to renal impairment, left hand, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3421');

-- Chronic gout due to renal impairment, left hand, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3421');

-- Chronic gout due to renal impairment, unspecified hand (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.349');

-- Chronic gout due to renal impairment, unspecified hand, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3490');

-- Chronic gout due to renal impairment, unspecified hand, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3491');

-- Chronic gout due to renal impairment, unspecified hand, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3491');

-- Chronic gout due to renal impairment, hip (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.35');

-- Chronic gout due to renal impairment, right hip (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.351');

-- Chronic gout due to renal impairment, right hip, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3510');

-- Chronic gout due to renal impairment, right hip, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3511');

-- Chronic gout due to renal impairment, right hip, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3511');

-- Chronic gout due to renal impairment, left hip (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.352');

-- Chronic gout due to renal impairment, left hip, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3520');

-- Chronic gout due to renal impairment, left hip, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3521');

-- Chronic gout due to renal impairment, left hip, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3521');

-- Chronic gout due to renal impairment, unspecified hip (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.359');

-- Chronic gout due to renal impairment, unspecified hip, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3590');

-- Chronic gout due to renal impairment, unspecified hip, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3591');

-- Chronic gout due to renal impairment, unspecified hip, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3591');

-- Chronic gout due to renal impairment, knee (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.36');

-- Chronic gout due to renal impairment, right knee (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.361');

-- Chronic gout due to renal impairment, right knee, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3610');

-- Chronic gout due to renal impairment, right knee, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3611');

-- Chronic gout due to renal impairment, right knee, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3611');

-- Chronic gout due to renal impairment, left knee (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.362');

-- Chronic gout due to renal impairment, left knee, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3620');

-- Chronic gout due to renal impairment, left knee, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3621');

-- Chronic gout due to renal impairment, left knee, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3621');

-- Chronic gout due to renal impairment, unspecified knee (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.369');

-- Chronic gout due to renal impairment, unspecified knee, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3690');

-- Chronic gout due to renal impairment, unspecified knee, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3691');

-- Chronic gout due to renal impairment, unspecified knee, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3691');

-- Chronic gout due to renal impairment, ankle and foot (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.37');

-- Chronic gout due to renal impairment, right ankle and foot (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.371');

-- Chronic gout due to renal impairment, right ankle and foot, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3710');

-- Chronic gout due to renal impairment, right ankle and foot, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3711');

-- Chronic gout due to renal impairment, right ankle and foot, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3711');

-- Chronic gout due to renal impairment, left ankle and foot (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.372');

-- Chronic gout due to renal impairment, left ankle and foot, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3720');

-- Chronic gout due to renal impairment, left ankle and foot, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3721');

-- Chronic gout due to renal impairment, left ankle and foot, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3721');

-- Chronic gout due to renal impairment, unspecified ankle and foot (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.379');

-- Chronic gout due to renal impairment, unspecified ankle and foot, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3790');

-- Chronic gout due to renal impairment, unspecified ankle and foot, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3791');

-- Chronic gout due to renal impairment, unspecified ankle and foot, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.3791');

-- Chronic gout due to renal impairment, vertebrae (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.38');

-- Chronic gout due to renal impairment, vertebrae, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.38X0');

-- Chronic gout due to renal impairment, vertebrae, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.38X1');

-- Chronic gout due to renal impairment, vertebrae, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.38X1');

-- Chronic gout due to renal impairment, multiple sites (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.39');

-- Chronic gout due to renal impairment, multiple sites, without tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.39X0');

-- Chronic gout due to renal impairment, multiple sites, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.39X1');

-- Chronic gout due to renal impairment, multiple sites, with tophus (tophi) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;gout;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'M1A.39X1');

-- Secondary hyperparathyroidism of renal origin (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;hyperparathyroidism;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N25.81');

-- Secondary hyperparathyroidism (of renal origin) (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;hyperparathyroidism;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '588.81');

-- Renal osteodystrophy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;osteodystrophy;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N25.0');

-- Renal osteodystrophy (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;renal_induced_disorder;osteodystrophy;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '588.0');

-- Nephropathy induced by other drugs, medicaments and biological substances (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;toxic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N14.1');

-- Nephropathy induced by unspecified drug, medicament or biological substance (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;toxic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N14.2');

-- Toxic nephropathy, not elsewhere classified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;toxic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N14.4');

-- Analgesic nephropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;toxic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N14.0');

-- Nephropathy induced by heavy metals (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;toxic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N14.3');

-- Drug- and heavy-metal-induced tubulo-interstitial and tubular conditions (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;toxic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N14');

-- Other renal tubulo-interstitial diseases (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;tubulo_interstit;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N15');

-- Other specified renal tubulo-interstitial diseases (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;tubulo_interstit;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N15.8');

-- Renal tubulo-interstitial disease, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;tubulo_interstit;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N15.9');

-- Renal tubulo-interstitial disorders in diseases classified elsewhere (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;moderate;tubulo_interstit;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N16');

-- Unspecified contracted kidney (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N26');

-- Atrophy of kidney (terminal) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N26.1');

-- Renal sclerosis, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N26.9');

-- Chronic kidney disease (CKD) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N18');

-- Chronic kidney disease, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N18.9');

-- Chronic kidney disease (CKD) (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '585');

-- Chronic kidney disease, unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '585.9');

-- Chronic kidney disease, stage 4 (severe) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;chronic;stage_4;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N18.4');

-- Chronic kidney disease, Stage IV (severe) (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;chronic;stage_4;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '585.4');

-- Chronic kidney disease, stage 5 (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;chronic;stage_5;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N18.5');

-- Chronic kidney disease, Stage V (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;chronic;stage_5;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '585.5');

-- Nephritis and nephropathy, not specified as acute or chronic, with lesion of renal cortical necrosis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;cortical_necrosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '583.6');

-- Nephritis and nephropathy, not specified as acute or chronic, with lesion of renal cortical necrosis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;cortical_necrosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '583.6');

-- Recurrent and persistent hematuria with diffuse crescentic glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;crescentic_glomerunonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N02.7');

-- Acute nephritic syndrome with diffuse crescentic glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;crescentic_glomerunonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N00.7');

-- Rapidly progressive nephritic syndrome (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;crescentic_glomerunonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N01');

-- Rapidly progressive nephritic syndrome with minor glomerular abnormality (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;crescentic_glomerunonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N01.0');

-- Rapidly progressive nephritic syndrome with focal and segmental glomerular lesions (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;crescentic_glomerunonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N01.1');

-- Rapidly progressive nephritic syndrome with diffuse membranous glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;crescentic_glomerunonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N01.2');

-- Rapidly progressive nephritic syndrome with diffuse mesangial proliferative glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;crescentic_glomerunonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N01.3');

-- Rapidly progressive nephritic syndrome with diffuse endocapillary proliferative glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;crescentic_glomerunonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N01.4');

-- Rapidly progressive nephritic syndrome with diffuse mesangiocapillary glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;crescentic_glomerunonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N01.5');

-- Rapidly progressive nephritic syndrome with dense deposit disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;crescentic_glomerunonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N01.6');

-- Rapidly progressive nephritic syndrome with diffuse crescentic glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;crescentic_glomerunonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N01.7');

-- Rapidly progressive nephritic syndrome with other morphologic changes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;crescentic_glomerunonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N01.8');

-- Rapidly progressive nephritic syndrome with unspecified morphologic changes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;crescentic_glomerunonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N01.9');

-- Chronic nephritic syndrome with diffuse crescentic glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;crescentic_glomerunonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N03.7');

-- Unspecified nephritic syndrome with diffuse crescentic glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;crescentic_glomerunonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N05.7');

-- Nephrotic syndrome with diffuse crescentic glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;crescentic_glomerunonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N04.7');

-- Isolated proteinuria with diffuse crescentic glomerulonephritis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;crescentic_glomerunonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N06.7');

-- Acute glomerulonephritis with lesion of rapidly progressive glomerulonephritis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;crescentic_glomerunonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '580.4');

-- Chronic glomerulonephritis with lesion of rapidly progressive glomerulonephritis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;crescentic_glomerunonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '582.4');

-- Nephritis and nephropathy, not specified as acute or chronic, with lesion of rapidly progressive glomerulonephritis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;crescentic_glomerunonephritis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '583.4');

-- End stage renal disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N18.6');

-- End stage renal disease (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '585.6');

-- Cloudy (hemodialysis) (peritoneal) dialysis effluent (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'R88.0');

-- Unspecified complication of foreign body accidentally left in body following kidney dialysis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T81.502');

-- Unspecified complication of foreign body accidentally left in body following kidney dialysis, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T81.502A');

-- Unspecified complication of foreign body accidentally left in body following kidney dialysis, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T81.502D');

-- Unspecified complication of foreign body accidentally left in body following kidney dialysis, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T81.502S');

-- Adhesions due to foreign body accidentally left in body following kidney dialysis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T81.512');

-- Adhesions due to foreign body accidentally left in body following kidney dialysis, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T81.512A');

-- Adhesions due to foreign body accidentally left in body following kidney dialysis, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T81.512D');

-- Adhesions due to foreign body accidentally left in body following kidney dialysis, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T81.512S');

-- Obstruction due to foreign body accidentally left in body following kidney dialysis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T81.522');

-- Obstruction due to foreign body accidentally left in body following kidney dialysis, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T81.522A');

-- Obstruction due to foreign body accidentally left in body following kidney dialysis, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T81.522D');

-- Obstruction due to foreign body accidentally left in body following kidney dialysis, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T81.522S');

-- Perforation due to foreign body accidentally left in body following kidney dialysis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T81.532');

-- Perforation due to foreign body accidentally left in body following kidney dialysis, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T81.532A');

-- Perforation due to foreign body accidentally left in body following kidney dialysis, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T81.532D');

-- Perforation due to foreign body accidentally left in body following kidney dialysis, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T81.532S');

-- Other complications of foreign body accidentally left in body following kidney dialysis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T81.592');

-- Other complications of foreign body accidentally left in body following kidney dialysis, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T81.592A');

-- Other complications of foreign body accidentally left in body following kidney dialysis, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T81.592D');

-- Other complications of foreign body accidentally left in body following kidney dialysis, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T81.592S');

-- Mechanical complication of vascular dialysis catheter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T82.4');

-- Breakdown (mechanical) of vascular dialysis catheter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T82.41');

-- Breakdown (mechanical) of vascular dialysis catheter, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T82.41XA');

-- Breakdown (mechanical) of vascular dialysis catheter, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T82.41XD');

-- Breakdown (mechanical) of vascular dialysis catheter, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T82.41XS');

-- Displacement of vascular dialysis catheter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T82.42');

-- Displacement of vascular dialysis catheter, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T82.42XA');

-- Displacement of vascular dialysis catheter, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T82.42XD');

-- Displacement of vascular dialysis catheter, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T82.42XS');

-- Leakage of vascular dialysis catheter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T82.43');

-- Leakage of vascular dialysis catheter, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T82.43XA');

-- Leakage of vascular dialysis catheter, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T82.43XD');

-- Leakage of vascular dialysis catheter, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T82.43XS');

-- Other complication of vascular dialysis catheter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T82.49');

-- Other complication of vascular dialysis catheter, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T82.49XA');

-- Other complication of vascular dialysis catheter, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T82.49XD');

-- Other complication of vascular dialysis catheter, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T82.49XS');

-- Breakdown (mechanical) of intraperitoneal dialysis catheter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.611');

-- Breakdown (mechanical) of intraperitoneal dialysis catheter, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.611A');

-- Breakdown (mechanical) of intraperitoneal dialysis catheter, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.611D');

-- Breakdown (mechanical) of intraperitoneal dialysis catheter, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.611S');

-- Displacement of intraperitoneal dialysis catheter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.621');

-- Displacement of intraperitoneal dialysis catheter, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.621A');

-- Displacement of intraperitoneal dialysis catheter, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.621D');

-- Displacement of intraperitoneal dialysis catheter, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.621S');

-- Leakage of intraperitoneal dialysis catheter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.631');

-- Leakage of intraperitoneal dialysis catheter, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.631A');

-- Leakage of intraperitoneal dialysis catheter, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.631D');

-- Leakage of intraperitoneal dialysis catheter, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.631S');

-- Other mechanical complication of intraperitoneal dialysis catheter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.691');

-- Other mechanical complication of intraperitoneal dialysis catheter, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.691A');

-- Other mechanical complication of intraperitoneal dialysis catheter, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.691D');

-- Other mechanical complication of intraperitoneal dialysis catheter, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.691S');

-- Infection and inflammatory reaction due to peritoneal dialysis catheter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.71');

-- Infection and inflammatory reaction due to peritoneal dialysis catheter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.71');

-- Infection and inflammatory reaction due to peritoneal dialysis catheter, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.71XA');

-- Infection and inflammatory reaction due to peritoneal dialysis catheter, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.71XA');

-- Infection and inflammatory reaction due to peritoneal dialysis catheter, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.71XD');

-- Infection and inflammatory reaction due to peritoneal dialysis catheter, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.71XD');

-- Infection and inflammatory reaction due to peritoneal dialysis catheter, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.71XS');

-- Failure of sterile precautions during kidney dialysis and other perfusion (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Y62.2');

-- Failure of sterile precautions during kidney dialysis and other perfusion (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Y62.2');

-- Kidney dialysis as the cause of abnormal reaction of the patient, or of later complication, without mention of misadventure at the time of the procedure (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Y84.1');

-- Encounter for aftercare following kidney transplant (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z48.22');

-- Encounter for care involving renal dialysis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z49');

-- Preparatory care for renal dialysis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z49.0');

-- Encounter for fitting and adjustment of extracorporeal dialysis catheter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z49.01');

-- Encounter for fitting and adjustment of peritoneal dialysis catheter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z49.02');

-- Encounter for adequacy testing for dialysis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z49.3');

-- Encounter for adequacy testing for hemodialysis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z49.31');

-- Encounter for adequacy testing for peritoneal dialysis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z49.32');

-- Patient's noncompliance with renal dialysis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z91.15');

-- Dependence on renal dialysis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z99.2');

-- Hypotension of hemodialysis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'I95.3');

-- Cloudy (hemodialysis) (peritoneal) dialysis effluent (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '792.5');

-- Mechanical complication due to peritoneal dialysis catheter (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '996.56');

-- Infection and inflammatory reaction due to peritoneal dialysis catheter (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '996.68');

-- Other complications due to renal dialysis device, implant, and graft (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '996.73');

-- Accidental cut, puncture, perforation or hemorrhage during kidney dialysis or other perfusion (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'E870.2');

-- Foreign object left in body during kidney dialysis or other perfusion (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'E871.2');

-- Failure of sterile precautions during kidney dialysis and other perfusion (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'E872.2');

-- Mechanical failure of instrument or apparatus during kidney dialysis and other perfusion (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'E874.2');

-- Kidney dialysis as the cause of abnormal reaction of patient, or of later complication, without mention of misadventure at time of procedure (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'E879.1');

-- Renal dialysis status (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'V45.11');

-- Noncompliance with renal dialysis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'V45.12');

-- Encounter for dialysis and dialysis catheter care (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'V56');

-- Encounter for extracorporeal dialysis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'V56.0');

-- Fitting and adjustment of extracorporeal dialysis catheter (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'V56.1');

-- Fitting and adjustment of peritoneal dialysis catheter (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'V56.2');

-- Encounter for adequacy testing for dialysis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'V56.3');

-- Encounter for adequacy testing for hemodialysis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'V56.31');

-- Encounter for adequacy testing for peritoneal dialysis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;dialysis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'V56.32');

-- Complications of kidney transplant (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;transplant;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T86.1');

-- Unspecified complication of kidney transplant (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;transplant;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T86.10');

-- Kidney transplant rejection (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;transplant;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T86.11');

-- Kidney transplant failure (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;transplant;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T86.12');

-- Kidney transplant infection (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;transplant;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T86.13');

-- Other complication of kidney transplant (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;transplant;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T86.19');

-- Kidney transplant status (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;transplant;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z94.0');

-- Complications of transplanted kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;transplant;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '996.81');

-- Kidney replaced by transplant (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;esrd;transplant;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'V42.0');

-- Acute kidney failure (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;failure;acute;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N17');

-- Acute kidney failure with tubular necrosis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;failure;acute;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N17.0');

-- Acute kidney failure with acute cortical necrosis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;failure;acute;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N17.1');

-- Acute kidney failure with medullary necrosis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;failure;acute;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N17.2');

-- Other acute kidney failure (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;failure;acute;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N17.8');

-- Acute kidney failure, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;failure;acute;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N17.9');

-- Acute kidney failure (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;failure;acute;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '584');

-- Acute kidney failure with lesion of tubular necrosis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;failure;acute;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '584.5');

-- Acute kidney failure with lesion of renal cortical necrosis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;failure;acute;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '584.6');

-- Acute kidney failure with lesion of renal medullary [papillary] necrosis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;failure;acute;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '584.7');

-- Acute kidney failure with other specified pathological lesion in kidney (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;failure;acute;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '584.8');

-- Acute kidney failure, unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;failure;acute;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '584.9');

-- Unspecified kidney failure (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;failure;unspecified;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'N19');

-- Renal failure, unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;failure;unspecified;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '586');

-- Nephritis and nephropathy, not specified as acute or chronic, with lesion of renal medullary necrosis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;medullary_necrosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '583.7');

-- Nephritis and nephropathy, not specified as acute or chronic, with lesion of renal medullary necrosis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;medullary_necrosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '583.7');

-- Renal sclerosis, unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;kidney_disorder;diagnosis;severe;sclerosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '587');

-- Acute psychoses (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;acute_psychosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F23%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;acute_psychosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F24%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;acute_psychosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F28%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;acute_psychosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F29%');

-- Acute psychoses (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;acute_psychosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.3');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;acute_psychosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;acute_psychosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '298.[013489]');

-- Alcohol dependence or abuse (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;alcohol_depend_abus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F10.1%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;alcohol_depend_abus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F10.2%');

-- Alcohol dependence or abuse (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;alcohol_depend_abus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '303%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;alcohol_depend_abus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '305.0%');

-- Alcohol-induced mental disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;alcohol_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F10.1%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;alcohol_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F10.2%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;alcohol_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F10.9%');

-- Alcohol-induced mental disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;alcohol_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '291%');

-- Anxiety and dissociative disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F40%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F41%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F42%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F44%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F48%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F68%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F55%');

-- Anxiety and dissociative disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '300.[0123569]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '298.2');

-- NO_NAME (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F41.3');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F41.8');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F41.9');

-- NO_NAME (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '300.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;anxiety_other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '300.09');

-- Childhood and adolescence disorders (behavioral, emotional) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;child_adolesc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F92%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;child_adolesc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F93%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;child_adolesc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F94%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;child_adolesc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F98%');

-- Childhood and adolescence disorders (behavioral, emotional) (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;child_adolesc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '313%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;child_adolesc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '307.[67]');

-- Conduct disorders and impulse disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;conduct_impuls;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F91%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;conduct_impuls;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F63%');

-- Conduct disorders and impulse disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;conduct_impuls;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '312%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;conduct_impuls;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '313.81');

-- Delirium (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F05');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F10.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F10.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F10.231');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F10.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F11.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F11.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F11.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F12.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F12.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F12.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F13.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F13.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F13.231');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F13.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F13.931');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F14.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F14.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F14.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F15.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F15.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F15.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F16.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F16.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F16.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F18.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F18.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F18.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F19.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F19.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F19.231');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F19.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F19.931');

-- Delirium (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '293.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '293.1');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '291.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '292.81');

-- Specific delays in development: (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F80%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F81%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F82%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F88%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F89%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'R48.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'H93.25');

-- Specific delays in development: (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '315%');

-- Drug dependence or abuse (except alcohol) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;drug_depend_abus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F1[1-9].[12]%');

-- Drug dependence or abuse (except alcohol) (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;drug_depend_abus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '304%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;drug_depend_abus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '305.[1-9]%');

-- Drug-induced mental disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F1[1-9].[129]%');

-- Drug-induced mental disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '292%');

-- Eating disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;eating;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F50%');

-- Eating disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;eating;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '307.1');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;eating;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '307.5%');

-- Generalized anxiety disorder (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;gad;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F41.1');

-- Generalized anxiety disorder (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;gad;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '300.02');

-- Hyperkinetic syndrome of childhood: (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;hyperkinet;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F90%');

-- Hyperkinetic syndrome of childhood: (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;hyperkinet;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '314%');

-- Motor disorders (tics) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;motor;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F95%');

-- Motor disorders (tics) (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;motor;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '307.[023]%');

-- Opioid related disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;opioid_abuse_depend;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F11%');

-- Opioid abuse and opioid dependence (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;opioid_abuse_depend;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '305.5%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;opioid_abuse_depend;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '304.0%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;opioid_abuse_depend;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '304.7%');

-- Other and unspecified mood disorder (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;other_mood;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F34%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;other_mood;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F39');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;other_mood;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.81');

-- Other and unspecified mood disorder (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;other_mood;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.9%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;other_mood;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '301.1%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;other_mood;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '300.4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;other_mood;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '625.4');

-- Panic disorder without agoraphobia (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;panic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F41.0');

-- Panic disorder without agoraphobia (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;panic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '300.01');

-- Personality disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;personality;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F21%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;personality;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F60%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;personality;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F68%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;personality;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F69%');

-- Personality disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;personality;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '301.[023456789]%');

-- Pregnancy and delivery related mental disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;pregnancy_delivery;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O90.6');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;pregnancy_delivery;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'O99.34%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;pregnancy_delivery;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F53');

-- Pregnancy and delivery related mental disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;pregnancy_delivery;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '648.4%');

-- Psychosomatic disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F42.4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F45%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F52.5');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F54%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F59%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'G44.209');

-- Psychosomatic disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '306%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '300.7');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '300.8%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '316%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '307.8%');

-- Intentional self-harm (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;self_harm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'X7[1-9]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;self_harm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'X8[0-3]%');

-- Intentional self-harm (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;self_harm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO 'E95[0-9]%');

-- Intentional self-harm (SNOMED) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;self_harm;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4244894);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;self_harm;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(439235);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;self_harm;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4303690);

-- Sexual and gender identity disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;sexual;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F52%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;sexual;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F64%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;sexual;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F65%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;sexual;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F66%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;sexual;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z87.890');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;sexual;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'R37');

-- Sexual and gender identity disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;sexual;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '302%');

-- Sleep disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;sleep;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F51%');

-- Sleep disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;sleep;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '307.4%');

-- Sleep disorders (SNOMED) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;sleep;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(435524);

-- Acute reaction to stress and adjustment disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;stress;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F43%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;stress;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'R45.7');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;stress;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F93.0');

-- Acute reaction to stress and adjustment disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;stress;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '308%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mental_comorb;stress;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '309%');

-- bipolar I, no remission codes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;bipolar_i;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F30.[12349]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;bipolar_i;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F31.[0-7]%');

-- bipolar I, no remission codes (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;bipolar_i;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[01456]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;bipolar_i;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.7');

-- bipolar II, no remission codes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;bipolar_ii;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.81');

-- bipolar II, no remission codes (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;bipolar_ii;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.89');

-- bipolar NOS, no remission codes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;bipolar_nos;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.8');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;bipolar_nos;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.89');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;bipolar_nos;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.9');

-- bipolar NOS, no remission codes (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;bipolar_disorder;bipolar_nos;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.8[0-2]');

-- MDD, including remission (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;major_depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F32%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;major_depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F33%');

-- MDD, inclusing remission (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;major_depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[23]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;major_depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '298.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;mmi;bipolar_spectrum;major_depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '311');

-- all psychiatric diagnoses (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;psychiatric_diagnoses;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F%');

-- all psychiatric diagnoses (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;psychiatric_diagnoses;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '29%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;psychiatric_diagnoses;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '30%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;psychiatric_diagnoses;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '31%');

-- pregnancy with abortive outcome (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;abortion;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40539858);

-- cardiac arrhythmia (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;arrhytmia;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44784217);

-- autoimmune (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;autoimmune;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(434621);

-- cardiovascular (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;cardiovascular;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(134057);

-- central nervous system (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;central_nerv_system;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(376106);

-- chronic infectious disease (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;chronic_infect_disease;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4073287);

-- chronic pain (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;chronic_pain;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(436096);

-- patient currently pregnant (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;currently_pregnant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4299535);

-- Dehydration (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;dehydration;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(435796);

-- delivery and perinatal care (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;delivery_perinatal_care;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4038495);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;delivery_perinatal_care;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4237498);

-- demyelinating disease of CNS (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;demyelinat;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(375801);

-- dermatological (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;dermatological;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4317258);

-- disorder of digestive tract (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;digestive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4309188);

-- endocrinopathy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;endocrinopathy;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(31821);

-- Disorder of glucose metabolism/regulation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;glucose_metabolism_regulation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4130526);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;glucose_metabolism_regulation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4130161);

-- HIV/AIDS (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;hiv_aids;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4241530);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;hiv_aids;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4221489);

-- hypertension (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;hypertension;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(316866);

-- injury (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'S%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'T%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'V%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'W%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'X[0-5]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'X[9][2-9]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'Y[2-3]%');

-- injury (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '8[0-9]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '9[0-8]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '99[0-5]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO 'E[0-8]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO 'E9[0-4]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO 'E9[6-9]%');

-- kidney (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;kidney;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(198124);

-- liver disease (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;liver;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(194984);

-- metabolic (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;metabolic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(436670);

-- Toxic effect of metals (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;metal;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'T56%');

-- Toxic effect of other metals (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;metal;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '985%');

-- medication-induced movement disorder (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;movement_medicat;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4186461);

-- musculoskeletal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;musculoskeletal;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4244662);

-- neoplasm and/or hamartoma (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;neoplasm_hamartoma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4266186);

-- nervous system (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;nerv_system;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(376337);

-- disorder of neuromuscular transmission (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;neuromusc_transmission;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4103776);

-- obesity, BMI 30 and more in adults (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;obesity_and_overweight;obesity;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'E66.[01289]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;obesity_and_overweight;obesity;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'Z68.[34]%');

-- obesity, BMI 30 and more in adults (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;obesity_and_overweight;obesity;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '278.00');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;obesity_and_overweight;obesity;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '278.01');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;obesity_and_overweight;obesity;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '278.03');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;obesity_and_overweight;obesity;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO 'V85.[34]%');

-- obesity     - SNOMED disorder (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;obesity_and_overweight;obesity;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(433736);

-- overweight, BMI 25-29.9 in adults (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;obesity_and_overweight;overweight;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E66.3');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;obesity_and_overweight;overweight;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'Z68.2[5-9]%');

-- overweight, BMI 25-29.9 in adults (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;obesity_and_overweight;overweight;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '278.02');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;obesity_and_overweight;overweight;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE 'V85.2%');

-- overweight, BMI 25-29.9 in adults - finding from SNOMED (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;obesity_and_overweight;overweight;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(437525);

-- post-streptococcal disorder (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;post_streptococ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4051204);

-- pulmonary (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;pulmon;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(257907);

-- rheumatism (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;rheumatism;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(138845);

-- seizure disorder (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;seizure;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4029498);

-- sexually transmitted infectious disease (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;sexually_transmitted;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(440647);

-- thyroidism (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;thyroid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(141253);

-- Toxic effects of substances chiefly nonmedicinal as to source (excluding alcohol) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;toxic_effect;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'T5[2-9]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;toxic_effect;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'T6[0-5]%');

-- Toxic effects of substances chiefly nonmedicinal as to source (excluding alcohol) (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;toxic_effect;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '98.[1-9]%');

-- tuberculosis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;tuberculosis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(434557);

-- Urinary incontinence of non-organic origin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'diagnoses;somatic_comorb;urinary_incontinence;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4172646);
