
-- Positive microalbuminuria test result documented and reviewed (DM) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106273);

-- Positive macroalbuminuria test result documented and reviewed (DM) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106276);

-- Documentation of treatment for nephropathy (eg, patient receiving dialysis, patient being treated for ESRD, CRF, ARF, or renal insufficiency, any visit to a nephrologist) (DM) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;exclusion;kidney;kidney_disorder_secondary;diabetes;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106278);

-- Nephrolithotomy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;calculus;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45890419);

-- Percutaneous nephrostolithotomy or pyelostolithotomy, with or without dilation, endoscopy, lithotripsy, stenting, or basket extraction (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;calculus;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45888012);

-- Lithotripsy and Ablation Procedures on the Kidney (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;calculus;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45889408);

-- Nephrolithotomy; removal of calculus (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;calculus;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2109553);

-- Nephrolithotomy; secondary surgical operation for calculus (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;calculus;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2109554);

-- Nephrolithotomy; complicated by congenital kidney abnormality (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;calculus;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2109555);

-- Nephrolithotomy; removal of large staghorn calculus filling renal pelvis and calyces (including anatrophic pyelolithotomy) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;calculus;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2109556);

-- Percutaneous nephrostolithotomy or pyelostolithotomy, with or without dilation, endoscopy, lithotripsy, stenting, or basket extraction; up to 2 cm (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;calculus;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2109557);

-- Percutaneous nephrostolithotomy or pyelostolithotomy, with or without dilation, endoscopy, lithotripsy, stenting, or basket extraction; over 2 cm (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;calculus;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2109558);

-- Pyelotomy; with removal of calculus (pyelolithotomy, pelviolithotomy, including coagulum pyelolithotomy) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;calculus;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2109563);

-- Global fee for extracorporeal shock wave lithotripsy treatment of kidney stone(s) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;calculus;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721045);

-- Extracorporeal shockwave lithotripsy [ESWL] of the kidney, ureter and/or bladder (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;calculus;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2008219);

-- Face-to-face educational services related to the care of chronic kidney disease; individual, per session, per one hour (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;chronic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40660436);

-- Face-to-face educational services related to the care of chronic kidney disease; group, per session, per one hour (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;chronic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40662352);

-- I intend to report the chronic kidney disease (ckd) measures group (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;chronic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617886);

-- All quality actions for the applicable measures in the chronic kidney disease (ckd) measures group have been performed for this patient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;chronic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617894);

-- Documentation of diagnosis of chronic kidney disease (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;chronic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40664728);

-- End-Stage Renal Disease Services (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45887996);

-- End-stage renal disease (ESRD) related services monthly, for patients younger than 2 years of age to include monitoring for the adequacy of nutrition, assessment of growth and development, and counseling of parents (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45890108);

-- End-stage renal disease (ESRD) related services monthly, for patients 2-11 years of age to include monitoring for the adequacy of nutrition, assessment of growth and development, and counseling of parents (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45887658);

-- End-stage renal disease (ESRD) related services monthly, for patients 12-19 years of age to include monitoring for the adequacy of nutrition, assessment of growth and development, and counseling of parents (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45890479);

-- End-stage renal disease (ESRD) related services monthly, for patients 20 years of age and older (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45889533);

-- End stage renal disease (ESRD) related services per full month; for patients under two years of age to include monitoring for the adequacy of nutrition, assessment of growth and development, and counseling of parents (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42738336);

-- End stage renal disease (ESRD) related services per full month; for patients between two and eleven years of age to include monitoring for the adequacy of nutrition, assessment of growth and development, and counseling of parents (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42738337);

-- End stage renal disease (ESRD) related services per full month; for patients between twelve and nineteen years of age to include monitoring for the adequacy of nutrition, assessment of growth and development, and counseling of parents (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42738338);

-- End stage renal disease (ESRD) related services per full month; for patients twenty years of age and over (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42738339);

-- End stage renal disease (ESRD) related services (less than full month), per day; for patients under two years of age (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42738676);

-- End stage renal disease (ESRD) related services (less than full month), per day; for patients between two and eleven years of age (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42738677);

-- End stage renal disease (ESRD) related services (less than full month), per day; for patients between twelve and nineteen years of age (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42738678);

-- End stage renal disease (ESRD) related services (less than full month), per day; for patients twenty years of age and over (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42738679);

-- End-stage renal disease (ESRD) related services monthly, for patients younger than 2 years of age to include monitoring for the adequacy of nutrition, assessment of growth and development, and counseling of parents; with 4 or more face-to-face visits by a (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213578);

-- End-stage renal disease (ESRD) related services monthly, for patients younger than 2 years of age to include monitoring for the adequacy of nutrition, assessment of growth and development, and counseling of parents; with 2-3 face-to-face visits by a physi (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213579);

-- End-stage renal disease (ESRD) related services monthly, for patients younger than 2 years of age to include monitoring for the adequacy of nutrition, assessment of growth and development, and counseling of parents; with 1 face-to-face visit by a physicia (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213580);

-- End-stage renal disease (ESRD) related services monthly, for patients 2-11 years of age to include monitoring for the adequacy of nutrition, assessment of growth and development, and counseling of parents; with 4 or more face-to-face visits by a physician (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213581);

-- End-stage renal disease (ESRD) related services monthly, for patients 2-11 years of age to include monitoring for the adequacy of nutrition, assessment of growth and development, and counseling of parents; with 2-3 face-to-face visits by a physician or ot (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213582);

-- End-stage renal disease (ESRD) related services monthly, for patients 2-11 years of age to include monitoring for the adequacy of nutrition, assessment of growth and development, and counseling of parents; with 1 face-to-face visit by a physician or other (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213583);

-- End-stage renal disease (ESRD) related services monthly, for patients 12-19 years of age to include monitoring for the adequacy of nutrition, assessment of growth and development, and counseling of parents; with 4 or more face-to-face visits by a physicia (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213584);

-- End-stage renal disease (ESRD) related services monthly, for patients 12-19 years of age to include monitoring for the adequacy of nutrition, assessment of growth and development, and counseling of parents; with 2-3 face-to-face visits by a physician or o (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213585);

-- End-stage renal disease (ESRD) related services monthly, for patients 12-19 years of age to include monitoring for the adequacy of nutrition, assessment of growth and development, and counseling of parents; with 1 face-to-face visit by a physician or othe (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213586);

-- End-stage renal disease (ESRD) related services monthly, for patients 20 years of age and older; with 4 or more face-to-face visits by a physician or other qualified health care professional per month (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213587);

-- End-stage renal disease (ESRD) related services monthly, for patients 20 years of age and older; with 2-3 face-to-face visits by a physician or other qualified health care professional per month (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213588);

-- End-stage renal disease (ESRD) related services monthly, for patients 20 years of age and older; with 1 face-to-face visit by a physician or other qualified health care professional per month (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213589);

-- End-stage renal disease (ESRD) related services for home dialysis per full month, for patients younger than 2 years of age to include monitoring for the adequacy of nutrition, assessment of growth and development, and counseling of parents (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213590);

-- End-stage renal disease (ESRD) related services for home dialysis per full month, for patients 2-11 years of age to include monitoring for the adequacy of nutrition, assessment of growth and development, and counseling of parents (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213591);

-- End-stage renal disease (ESRD) related services for home dialysis per full month, for patients 12-19 years of age to include monitoring for the adequacy of nutrition, assessment of growth and development, and counseling of parents (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213592);

-- End-stage renal disease (ESRD) related services for home dialysis per full month, for patients 20 years of age and older (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213593);

-- End-stage renal disease (ESRD) related services for dialysis less than a full month of service, per day; for patients younger than 2 years of age (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213594);

-- End-stage renal disease (ESRD) related services for dialysis less than a full month of service, per day; for patients 2-11 years of age (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213595);

-- End-stage renal disease (ESRD) related services for dialysis less than a full month of service, per day; for patients 12-19 years of age (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213596);

-- End-stage renal disease (ESRD) related services for dialysis less than a full month of service, per day; for patients 20 years of age and older (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213597);

-- Adjustable chair, for esrd patients (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616970);

-- ESRD related svc 4+mo < 2yrs (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617386);

-- ESRD related svc 2-3mo <2yrs (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617387);

-- ESRD related svc 1 vst <2yrs (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617388);

-- ESRD related svs 4+mo 2-11yr (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617389);

-- ESRD relate svs 2-3 mo 2-11y (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617390);

-- ESRD related svs 1 mon 2-11y (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617391);

-- ESRD related svs 4+ mo 12-19 (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617392);

-- ESRD related svs 2-3mo/12-19 (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617393);

-- ESRD related svs 1vis/12-19y (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617394);

-- END STAGE RENAL DISEASE (ESRD) RELATED SERVICES DURING THE COURSE OF TREATMENT, FOR PATIENTS 20 YEARS OF AGE AND OVER; WITH 4 OR MORE FACE-TO-FACE PHYSICIAN VISITS PER MONTH (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617395);

-- END STAGE RENAL DISEASE (ESRD) RELATED SERVICES DURING THE COURSE OF TREATMENT, FOR PATIENTS 20 YEARS OF AGE AND OVER; WITH 2 OR 3 FACE-TO-FACE PHYSICIAN VISITS PER MONTH (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617396);

-- END STAGE RENAL DISEASE (ESRD) RELATED SERVICES DURING THE COURSE OF TREATMENT, FOR PATIENTS 20 YEARS OF AGE AND OVER; WITH 1 FACE-TO-FACE PHYSICIAN VISIT PER MONTH (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617397);

-- END STAGE RENAL DISEASE (ESRD) RELATED SERVICES LESS THAN FULL MONTH, PER DAY; FOR PATIENTS UNDER TWO YEARS OF AGE (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617402);

-- END STAGE RENAL DISEASE (ESRD) RELATED SERVICES LESS THAN FULL MONTH, PER DAY; FOR PATIENTS BETWEEN TWO AND ELEVEN YEARS OF AGE (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617403);

-- END STAGE RENAL DISEASE (ESRD) RELATED SERVICES LESS THAN FULL MONTH, PER DAY; FOR PATIENTS BETWEEN TWELVE AND NINETEEN YEARS OF AGE (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617404);

-- END STAGE RENAL DISEASE (ESRD) RELATED SERVICES LESS THAN FULL MONTH, PER DAY; FOR PATIENTS TWENTY YEARS OF AGE AND OVER (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617405);

-- Clinician documented that end stage renal disease patient was not an eligible candidate for urr or kt/v measure (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617546);

-- End stage renal disease patient with documented hematocrit greater than or equal to 33 (or hemoglobin greater than or equal to 11) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617547);

-- End stage renal disease patient with documented hematocrit less than 33 (or hemoglobin less than 11) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617548);

-- Clinician documented that end stage renal disease patient was not an eligible candidate for hematocrit (hemoglobin) measure (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617549);

-- End stage renal disease patient requiring hemodialysis vascular access documented to have received autogenous av fistula (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617550);

-- End stage renal disease patient requiring hemodialysis documented to have received vascular access other than autogenous av fistula (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617551);

-- End-stage renal disease patient requiring hemodialysis vascular access was not an eligible candidate for autogenous av fistula (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617552);

-- End-stage renal disease patient with a hematocrit or hemoglobin not documented (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617794);

-- End-stage renal disease patient with urr or kt/v value not documented, but otherwise eligible for measure (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617795);

-- Clinician intends to report the end stage renal disease (esrd) measure group (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617887);

-- Esrd demo basic bundle level i (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617957);

-- Esrd demo expanded bundle including venous access and related services (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617958);

-- Documentation of end stage renal disease (esrd), dialysis, renal transplant or pregnancy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786436);

-- Item or service furnished to an esrd patient that is not for the treatment of esrd (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2615430);

-- Amcc test has been ordered by an esrd facility or mcp physician that is part of the composite rate and is not separately billable (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616471);

-- Amcc test has been ordered by an esrd facility or mcp physician that is a composite rate test but is beyond the normal frequency covered under the rate and is separately reimbursable based on medical necessity (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616472);

-- Amcc test has been ordered by an esrd facility or mcp physician that is not part of the composite rate and is separately billable (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616473);

-- Emergency reserve supply (for esrd benefit only) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617154);

-- Hemodialysis plan of care documented (ESRD, P-ESRD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2101833);

-- Peritoneal dialysis plan of care documented (ESRD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2101834);

-- Hemodialysis Access, Intervascular Cannulation for Extracorporeal Circulation, or Shunt Insertion Procedures on Arteries and Veins (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45889749);

-- Insertion of cannula for hemodialysis, other purpose (separate procedure) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45889973);

-- Dialysis Services and Procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45889365);

-- Hemodialysis Procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45889034);

-- Miscellaneous Dialysis Services and Procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45888999);

-- Other Dialysis Procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45888492);

-- End-stage renal disease (ESRD) related services for dialysis less than a full month of service, per day (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45889335);

-- Introduction of needle and/or catheter, arteriovenous shunt created for dialysis (graft/fistula) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45888223);

-- Anesthesia for vascular shunt, or shunt revision, any type (eg, dialysis) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2101765);

-- Introduction of needle or intracatheter; arteriovenous shunt created for dialysis (cannula, fistula, or graft) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42736574);

-- Introduction of needle and/or catheter, arteriovenous shunt created for dialysis (graft/fistula); initial access with complete radiological evaluation of dialysis access, including fluoroscopy, image documentation and report (includes access of shunt, inj (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40756908);

-- Introduction of needle and/or catheter, arteriovenous shunt created for dialysis (graft/fistula); additional access for therapeutic intervention (List separately in addition to code for primary procedure) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40757115);

-- Thrombectomy, open, arteriovenous fistula without revision, autogenous or nonautogenous dialysis graft (separate procedure) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108297);

-- Revision, open, arteriovenous fistula; without thrombectomy, autogenous or nonautogenous dialysis graft (separate procedure) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108298);

-- Revision, open, arteriovenous fistula; with thrombectomy, autogenous or nonautogenous dialysis graft (separate procedure) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108299);

-- Distal revascularization and interval ligation (DRIL), upper extremity hemodialysis access (steal syndrome) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108302);

-- Hemodialysis via functioning arteriovenous (AV) fistula (ESRD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108564);

-- Hemodialysis via functioning arteriovenous (AV) graft (ESRD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108566);

-- Hemodialysis via catheter (ESRD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108567);

-- Patient receiving peritoneal dialysis (ESRD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108568);

-- Insertion of tunneled intraperitoneal catheter for dialysis, open (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2109463);

-- Angiography, arteriovenous shunt (eg, dialysis patient), radiological supervision and interpretation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42737977);

-- Angiography, arteriovenous shunt (eg, dialysis patient fistula/graft), complete evaluation of dialysis access, including fluoroscopy, image documentation and report (includes injections of contrast and all necessary imaging from the arterial anastomosis a (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40756855);

-- Hemodialysis procedure with single evaluation by a physician or other qualified health care professional (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213572);

-- Hemodialysis procedure requiring repeated evaluation(s) with or without substantial revision of dialysis prescription (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213573);

-- Hemodialysis access flow study to determine blood flow in grafts and arteriovenous fistulae by an indicator dilution method, hook-up; transcutaneous measurement and disconnection (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42739840);

-- Hemodialysis access flow study to determine blood flow in grafts and arteriovenous fistulae by an indicator method (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213575);

-- Dialysis procedure other than hemodialysis (eg, peritoneal dialysis, hemofiltration, or other continuous renal replacement therapies), with single evaluation by a physician or other qualified health care professional (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213576);

-- Dialysis procedure other than hemodialysis (eg, peritoneal dialysis, hemofiltration, or other continuous renal replacement therapies) requiring repeated evaluations by a physician or other qualified health care professional, with or without substantial re (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213577);

-- Dialysis training, patient, including helper where applicable, any mode, completed course (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213598);

-- Dialysis training, patient, including helper where applicable, any mode, course not completed, per training session (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213599);

-- Unlisted dialysis procedure, inpatient or outpatient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213601);

-- Duplex scan of hemodialysis access (including arterial inflow, body of access and venous outflow) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2313999);

-- Home visit for hemodialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2514586);

-- Home infusion of peritoneal dialysis, per diem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42739891);

-- Peritoneal dialysis catheter anchoring device, belt, each (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614963);

-- Disposable cycler set used with cycler dialysis machine, each (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614970);

-- Drainage extension line, sterile, for dialysis, each (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614971);

-- Extension line with easy lock connectors, used with dialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614972);

-- Chemicals/antiseptics solution used to clean/sterilize dialysis equipment, per 8 oz (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614973);

-- Activated carbon filter for hemodialysis, each (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614974);

-- Dialyzer (artificial kidneys), all types, all sizes, for hemodialysis, each (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614975);

-- Bicarbonate concentrate, solution, for hemodialysis, per gallon (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614978);

-- Bicarbonate concentrate, powder, for hemodialysis, per packet (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614979);

-- Acetate concentrate solution, for hemodialysis, per gallon (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614980);

-- Acid concentrate, solution, for hemodialysis, per gallon (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614981);

-- Treated water (deionized, distilled, or reverse osmosis) for peritoneal dialysis, per gallon (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614983);

-- y set tubing for peritoneal dialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614984);

-- Dialysate solution, any concentration of dextrose, fluid volume greater than 249cc, but less than or equal to 999 cc, for peritoneal dialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614985);

-- Dialysate solution, any concentration of dextrose, fluid volume greater than 999 cc but less than or equal to 1999 cc, for peritoneal dialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614986);

-- Dialysate solution, any concentration of dextrose, fluid volume greater than 1999 cc but less than or equal to 2999 cc, for peritoneal dialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614987);

-- Dialysate solution, any concentration of dextrose, fluid volume greater than 2999 cc but less than or equal to 3999 cc, for peritoneal dialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614988);

-- Dialysate solution, any concentration of dextrose, fluid volume greater than 3999 cc but less than or equal to 4999 cc, for peritoneal dialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614989);

-- Dialysate solution, any concentration of dextrose, fluid volume greater than 4999 cc but less than or equal to 5999 cc, for peritoneal dialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614990);

-- Dialysate solution, any concentration of dextrose, fluid volume greater than 5999 cc, for peritoneal dialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614991);

-- Fistula cannulation set for hemodialysis, each (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614993);

-- Topical anesthetic, for dialysis, per gram (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614995);

-- Injectable anesthetic, for dialysis, per 10 ml (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614996);

-- Shunt accessory, for hemodialysis, any type, each (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614997);

-- Blood tubing, arterial or venous, for hemodialysis, each (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614998);

-- Blood tubing, arterial and venous combined, for hemodialysis, each (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614999);

-- Dialysate solution test kit, for peritoneal dialysis, any type, each (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2615000);

-- Dialysate concentrate, powder, additive for peritoneal dialysis, per packet (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2615001);

-- Dialysate concentrate, solution, additive for peritoneal dialysis, per 10 ml (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2615002);

-- Blood collection tube, vacuum, for dialysis, per 50 (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2615003);

-- Serum clotting time tube, for dialysis, per 50 (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2615004);

-- Blood glucose test strips, for dialysis, per 50 (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2615005);

-- Occult blood test strips, for dialysis, per 50 (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2615006);

-- Ammonia test strips, for dialysis, per 50 (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2615007);

-- Protamine sulfate, for hemodialysis, per 50 mg (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2615012);

-- Disposable catheter tips for peritoneal dialysis, per 10 (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2615015);

-- Plumbing and/or electrical work for home hemodialysis equipment (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2615016);

-- Contracts, repair and maintenance, for hemodialysis equipment (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2615018);

-- Drain bag/bottle, for dialysis, each (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2615023);

-- Miscellaneous dialysis supplies, not otherwise specified (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2615025);

-- Venous pressure clamp, for hemodialysis, each (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2615027);

-- Tourniquet for dialysis, each (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2615033);

-- Catheter, hemodialysis/peritoneal, long-term (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2615761);

-- Catheter, hemodialysis/peritoneal, short-term (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2615763);

-- Dialysis access system (implantable) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2615860);

-- Centrifuge, for dialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616963);

-- Kidney, dialysate delivery syst kidney machine, pump recirculating, air removal syst, flowrate meter, power off, heater and temperature control with alarm, i.v. poles, pressure gauge, concentrate container (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616964);

-- Heparin infusion pump for hemodialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616965);

-- Air bubble detector for hemodialysis, each, replacement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616966);

-- Pressure alarm for hemodialysis, each, replacement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616967);

-- Bath conductivity meter for hemodialysis, each (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616968);

-- Blood leak detector for hemodialysis, each, replacement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616969);

-- Transducer protectors/fluid barriers, for hemodialysis, any size, per 10 (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616971);

-- Unipuncture control system for hemodialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616972);

-- Hemodialysis machine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616973);

-- Automatic intermittent peritoneal dialysis system (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616974);

-- Cycler dialysis machine for peritoneal dialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616975);

-- Delivery and/or installation charges for hemodialysis equipment (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616976);

-- Reverse osmosis water purification system, for hemodialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616977);

-- Deionizer water purification system, for hemodialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616978);

-- Blood pump for hemodialysis, replacement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616979);

-- Water softening system, for hemodialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616980);

-- Reciprocating peritoneal dialysis system (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616981);

-- Wearable artificial kidney, each (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616982);

-- Peritoneal dialysis clamps, each (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616983);

-- Compact (portable) travel hemodialyzer system (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616984);

-- Sorbent cartridges, for hemodialysis, per 10 (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616985);

-- Dialysis equipment, not otherwise specified (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616990);

-- Unscheduled or emergency dialysis treatment for an esrd patient in a hospital outpatient department that is not certified as an esrd facility (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617342);

-- END STAGE RENAL DISEASE (ESRD) RELATED SERVICES FOR HOME DIALYSIS PATIENTS PER FULL MONTH; FOR PATIENTS UNDER TWO YEARS OF AGE TO INCLUDE MONITORING FOR ADEQUACY OF NUTRITION, ASSESSMENT OF GROWTH AND DEVELOPMENT, AND COUNSELING OF PARENTS (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617398);

-- END STAGE RENAL DISEASE (ESRD) RELATED SERVICES FOR HOME DIALYSIS PATIENTS PER FULL MONTH; FOR PATIENTS TWO TO ELEVEN YEARS OF AGE TO INCLUDE MONITORING FOR ADEQUACY OF NUTRITION, ASSESSMENT OF GROWTH AND DEVELOPMENT, AND COUNSELING OF PARENTS (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617399);

-- END STAGE RENAL DISEASE (ESRD) RELATED SERVICES FOR HOME DIALYSIS PATIENTS PER FULL MONTH; FOR PATIENTS TWELVE TO NINETEEN YEARS OF AGE TO INCLUDE MONITORING FOR ADEQUACY OF NUTRITION, ASSESSMENT OF GROWTH AND DEVELOPMENT, AND COUNSELING OF PARENTS (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617400);

-- END STAGE RENAL DISEASE (ESRD) RELATED SERVICES FOR HOME DIALYSIS PATIENTS PER FULL MONTH; FOR PATIENTS TWENTY YEARS OF AGE AND OLDER (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617401);

-- Vessel mapping of vessels for hemodialysis access (services for preoperative vessel mapping prior to creation of hemodialysis access using an autogenous hemodialysis conduit, including arterial inflow and venous outflow) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617440);

-- TRANSLUMINAL BALLOON ANGIOPLASTY, PERCUTANEOUS; FOR MAINTENANCE OF HEMODIALYSIS ACCESS, ARTERIOVENOUS FISTULA OR GRAFT; ARTERIAL (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617461);

-- TRANSLUMINAL BALLOON ANGIOPLASTY, PERCUTANEOUS; FOR MAINTENANCE OF HEMODIALYSIS ACCESS, ARTERIOVENOUS FISTULA OR GRAFT; VENOUS (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617462);

-- End stage renal disease patient with documented dialysis dose of urr greater than or equal to 65% (or kt/v greater than or equal to 1.2) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617544);

-- End stage renal disease patient with documented dialysis dose of urr less than 65% (or kt/v less than 1.2) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617545);

-- Spkt/v greater than or equal to 1.2 (single-pool clearance of urea [kt] / volume [v]) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40664884);

-- Hemodialysis treatment performed exactly three times per week for > 90 days (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40664693);

-- Hemodialysis treatment performed less than three times per week or greater than three times per week (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40664745);

-- Documentation of reason(s) for patient not having greater than or equal to 1.2 (single-pool clearance of urea [kt] / volume [v]) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40664835);

-- Spkt/v less than 1.2 (single-pool clearance of urea [kt] / volume [v]), reason not given (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40664668);

-- Total kt/v greater than or equal to 1.7 per week (total clearance of urea [kt] / volume [v]) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40664836);

-- Total kt/v less than 1.7 per week (total clearance of urea [kt] / volume [v]) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40664908);

-- Patient receiving hemodialysis, peritoneal dialysis or kidney transplantation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40664909);

-- Patient receiving maintenance hemodialysis in an outpatient dialysis facility (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533281);

-- Patient whose mode of vascular access is a catheter at the time maintenance hemodialysis is initiated (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786445);

-- Patient whose mode of vascular access is not a catheter at the time maintenance hemodialysis is initiated (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786446);

-- Patient receiving maintenance hemodialysis for greater than or equal to 90 days with a catheter as the mode of vascular access (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786470);

-- Patient receiving maintenance hemodialysis for greater than or equal to 90 days without a catheter as the mode of vascular access (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786471);

-- Patient discontinued from hemodialysis or peritoneal dialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(915649);

-- Injection, darbepoetin alfa, 1 microgram (for esrd on dialysis) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718324);

-- Injection, epoetin alfa, 1000 units (for esrd on dialysis) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718326);

-- Injection, epoetin beta, 1 microgram, (for esrd on dialysis) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45890797);

-- Injection, peginesatide, 0.1 mg (for esrd on dialysis) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533349);

-- Injection, ferumoxytol, for treatment of iron deficiency anemia, 1 mg (for esrd on dialysis) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40658868);

-- Injection, peginesatide, 0.1 mg (for esrd on dialysis) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533289);

-- Injection, epoetin alfa, 100 units (for esrd on dialysis) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2720773);

-- Injection, epoetin beta, 1 microgram, (for esrd on dialysis) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45890825);

-- Dialysis/stress vitamin supplement, oral, 100 capsules (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721007);

-- Home therapy, hemodialysis; administrative services, professional pharmacy services, care coordination, and all necessary supplies and equipment (drugs and nursing services coded separately), per diem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721479);

-- Home therapy; peritoneal dialysis, administrative services, professional pharmacy services, care coordination and all necessary supplies and equipment (drugs and nursing visits coded separately), per diem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721482);

-- Dialysis services (medicare fee schedule) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45754745);

-- Dialysis services (non-medicare fee schedule) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45754746);

-- Item furnished in conjunction with dialysis services (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2615429);

-- Service ordered by a renal dialysis facility (rdf) physician as part of the esrd beneficiary's dialysis benefit, is not part of the composite rate, and is separately reimbursable (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616469);

-- Esrd patient for whom less than six dialysis sessions have been provided in a month (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617493);

-- Venous catheterization for renal dialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2002176);

-- Arteriovenostomy for renal dialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2002189);

-- Revision of arteriovenous shunt for renal dialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2002208);

-- Removal of arteriovenous shunt for renal dialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2002209);

-- Hemodialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2002282);

-- Total body perfusion (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2002283);

-- Other perfusion (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2002284);

-- Peritoneal dialysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2003564);

-- Removal of mechanical kidney (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;esrd;dialysis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2003665);

-- Renal Transplantation Procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45888790);

-- Backbench reconstruction of cadaver or living donor renal allograft prior to transplantation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45887599);

-- Renal allotransplantation, implantation of graft (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45887600);

-- Backbench standard preparation of cadaver donor renal allograft prior to transplantation, including dissection and removal of perinephric fat, diaphragmatic and retroperitoneal attachments, excision of adrenal gland, and preparation of ureter(s), renal ve (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2109580);

-- Backbench standard preparation of living donor renal allograft (open or laparoscopic) prior to transplantation, including dissection and removal of perinephric fat and preparation of ureter(s), renal vein(s), and renal artery(s), ligating branches, as nec (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2109581);

-- Backbench reconstruction of cadaver or living donor renal allograft prior to transplantation; venous anastomosis, each (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2109582);

-- Backbench reconstruction of cadaver or living donor renal allograft prior to transplantation; arterial anastomosis, each (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2109583);

-- Backbench reconstruction of cadaver or living donor renal allograft prior to transplantation; ureteral anastomosis, each (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2109584);

-- Recipient nephrectomy (separate procedure) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2109585);

-- Renal allotransplantation, implantation of graft; without recipient nephrectomy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2109586);

-- Renal allotransplantation, implantation of graft; with recipient nephrectomy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2109587);

-- Removal of transplanted renal allograft (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2109588);

-- Ultrasound, transplanted kidney, real time and duplex Doppler with image documentation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2211744);

-- Ultrasound, transplanted kidney, B-scan and/or real time with image documentation, with or without duplex Doppler study (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42737578);

-- Simultaneous pancreas kidney transplantation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721092);

-- Removal of transplanted or rejected kidney (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2003622);

-- Transplant of kidney (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2003624);

-- Renal autotransplantation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2003625);

-- Other kidney transplantation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2003626);

-- Implantation or replacement of mechanical kidney (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2003664);

-- Replacement of Right Kidney Pelvis with Autologous Tissue Substitute, Open Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776260);

-- Replacement of Right Kidney Pelvis with Synthetic Substitute, Open Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776261);

-- Replacement of Right Kidney Pelvis with Nonautologous Tissue Substitute, Open Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776262);

-- Replacement of Right Kidney Pelvis with Autologous Tissue Substitute, Percutaneous Endoscopic Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776263);

-- Replacement of Right Kidney Pelvis with Synthetic Substitute, Percutaneous Endoscopic Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776264);

-- Replacement of Right Kidney Pelvis with Nonautologous Tissue Substitute, Percutaneous Endoscopic Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776265);

-- Replacement of Right Kidney Pelvis with Autologous Tissue Substitute, Via Natural or Artificial Opening (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776266);

-- Replacement of Right Kidney Pelvis with Synthetic Substitute, Via Natural or Artificial Opening (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776267);

-- Replacement of Right Kidney Pelvis with Nonautologous Tissue Substitute, Via Natural or Artificial Opening (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776268);

-- Replacement of Right Kidney Pelvis with Autologous Tissue Substitute, Via Natural or Artificial Opening Endoscopic (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776269);

-- Replacement of Right Kidney Pelvis with Synthetic Substitute, Via Natural or Artificial Opening Endoscopic (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776270);

-- Replacement of Right Kidney Pelvis with Nonautologous Tissue Substitute, Via Natural or Artificial Opening Endoscopic (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776271);

-- Replacement of Left Kidney Pelvis with Autologous Tissue Substitute, Open Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776272);

-- Replacement of Left Kidney Pelvis with Synthetic Substitute, Open Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776464);

-- Replacement of Left Kidney Pelvis with Nonautologous Tissue Substitute, Open Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776465);

-- Replacement of Left Kidney Pelvis with Autologous Tissue Substitute, Percutaneous Endoscopic Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776466);

-- Replacement of Left Kidney Pelvis with Synthetic Substitute, Percutaneous Endoscopic Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776467);

-- Replacement of Left Kidney Pelvis with Nonautologous Tissue Substitute, Percutaneous Endoscopic Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776468);

-- Replacement of Left Kidney Pelvis with Autologous Tissue Substitute, Via Natural or Artificial Opening (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776469);

-- Replacement of Left Kidney Pelvis with Synthetic Substitute, Via Natural or Artificial Opening (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776470);

-- Replacement of Left Kidney Pelvis with Nonautologous Tissue Substitute, Via Natural or Artificial Opening (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776471);

-- Replacement of Left Kidney Pelvis with Autologous Tissue Substitute, Via Natural or Artificial Opening Endoscopic (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776472);

-- Replacement of Left Kidney Pelvis with Synthetic Substitute, Via Natural or Artificial Opening Endoscopic (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776473);

-- Replacement of Left Kidney Pelvis with Nonautologous Tissue Substitute, Via Natural or Artificial Opening Endoscopic (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2776474);

-- Transplantation of Right Kidney, Allogeneic, Open Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2774517);

-- Transplantation of Right Kidney, Syngeneic, Open Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2774518);

-- Transplantation of Right Kidney, Zooplastic, Open Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2774519);

-- Transplantation of Left Kidney, Allogeneic, Open Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2774520);

-- Transplantation of Left Kidney, Syngeneic, Open Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2774521);

-- Transplantation of Left Kidney, Zooplastic, Open Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2774522);

-- Medical and Surgical @ Urinary System @ Change @ Kidney (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2844802);

-- Medical and Surgical @ Urinary System @ Change @ Kidney @ External (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2824918);

-- Medical and Surgical @ Urinary System @ Change @ Kidney @ External @ Drainage Device (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2844803);

-- Medical and Surgical @ Urinary System @ Change @ Kidney @ External @ Other Device (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2871409);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Right (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2850172);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Right @ Open (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2813004);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Right @ Open @ Autologous Tissue Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2876950);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Right @ Open @ Synthetic Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2876951);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Right @ Open @ Nonautologous Tissue Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2898426);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Right @ Percutaneous Endoscopic (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889903);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Right @ Percutaneous Endoscopic @ Autologous Tissue Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2832988);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Right @ Percutaneous Endoscopic @ Synthetic Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2837979);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Right @ Percutaneous Endoscopic @ Nonautologous Tissue Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2813005);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Right @ Via Natural or Artificial Opening (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2832989);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Right @ Via Natural or Artificial Opening @ Autologous Tissue Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2884881);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Right @ Via Natural or Artificial Opening @ Synthetic Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2832990);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Right @ Via Natural or Artificial Opening @ Nonautologous Tissue Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2850173);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Right @ Via Natural or Artificial Opening Endoscopic (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2863798);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Right @ Via Natural or Artificial Opening Endoscopic @ Autologous Tissue Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2827705);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Right @ Via Natural or Artificial Opening Endoscopic @ Synthetic Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2884882);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Right @ Via Natural or Artificial Opening Endoscopic @ Nonautologous Tissue Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2833114);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Left (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2813006);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Left @ Open (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2876952);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Left @ Open @ Autologous Tissue Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2850174);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Left @ Open @ Synthetic Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2813007);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Left @ Open @ Nonautologous Tissue Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2833115);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Left @ Percutaneous Endoscopic (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2850175);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Left @ Percutaneous Endoscopic @ Autologous Tissue Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2802091);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Left @ Percutaneous Endoscopic @ Synthetic Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2827706);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Left @ Percutaneous Endoscopic @ Nonautologous Tissue Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2802092);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Left @ Via Natural or Artificial Opening (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2833116);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Left @ Via Natural or Artificial Opening @ Autologous Tissue Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2804984);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Left @ Via Natural or Artificial Opening @ Synthetic Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2819674);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Left @ Via Natural or Artificial Opening @ Nonautologous Tissue Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2845158);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Left @ Via Natural or Artificial Opening Endoscopic (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889904);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Left @ Via Natural or Artificial Opening Endoscopic @ Autologous Tissue Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2845159);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Left @ Via Natural or Artificial Opening Endoscopic @ Synthetic Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2898427);

-- Medical and Surgical @ Urinary System @ Replacement @ Kidney Pelvis, Left @ Via Natural or Artificial Opening Endoscopic @ Nonautologous Tissue Substitute (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2813008);

-- Medical and Surgical @ Urinary System @ Transplantation @ Kidney, Right (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2877118);

-- Medical and Surgical @ Urinary System @ Transplantation @ Kidney, Right @ Open (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2799812);

-- Medical and Surgical @ Urinary System @ Transplantation @ Kidney, Right @ Open @ No Device (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2877119);

-- Medical and Surgical @ Urinary System @ Transplantation @ Kidney, Left (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2833286);

-- Medical and Surgical @ Urinary System @ Transplantation @ Kidney, Left @ Open (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2890060);

-- Medical and Surgical @ Urinary System @ Transplantation @ Kidney, Left @ Open @ No Device (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;kidney_disorder;procedure;transplant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2827873);

-- Unlisted psychiatric service or procedure (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213561);

-- Psychiatry Services and Procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45889674);

-- Other Psychiatric Services or Procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45887735);

-- Mental health services, not otherwise specified (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618145);

-- Psychiatric health facility service, per diem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618163);

-- Intensive outpatient psychiatric services, per diem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721547);

-- Mental health partial hospitalization, treatment, less than 24 hours (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618134);

-- Mental health service plan development by non-physician (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618131);

-- Mental Health (Procedure) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900282);

-- Mental Health @ None (Procedure) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894430);

-- Alcohol and drug rehabilitation and detoxification (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007785);

-- Alcohol rehabilitation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007786);

-- Alcohol detoxification (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007787);

-- Alcohol rehabilitation and detoxification (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007788);

-- Drug rehabilitation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007789);

-- Drug detoxification (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007790);

-- Drug rehabilitation and detoxification (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007791);

-- Combined alcohol and drug rehabilitation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007802);

-- Combined alcohol and drug detoxification (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007803);

-- Combined alcohol and drug rehabilitation and detoxification (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007804);

-- Drug addiction counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007765);

-- Alcoholism counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007766);

-- Referral for alcoholism rehabilitation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007771);

-- Referral for drug addiction rehabilitation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007782);

-- Alcohol and/or substance (other than tobacco) abuse structured screening (eg, AUDIT, DAST), and brief intervention (SBI) services; 15 to 30 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2514536);

-- Alcohol and/or substance (other than tobacco) abuse structured screening (eg, AUDIT, DAST), and brief intervention (SBI) services; greater than 30 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2514537);

-- Patient screened for unhealthy alcohol use using a systematic screening method (PV) (DSP) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106161);

-- Patient counseled regarding psychosocial and pharmacologic treatment options for opioid addiction (SUD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108850);

-- Patient counseled regarding psychosocial and pharmacologic treatment options for alcohol dependence (SUD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108871);

-- Alcohol and/or substance (other than tobacco) abuse structured screening (eg, AUDIT, DAST), and brief intervention (SBI) services (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45888215);

-- Patient screened for depression (SUD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2102214);

-- Alcohol and/or drug services; group counseling by a clinician (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618104);

-- Alcohol and/or drug services; case management (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618105);

-- Alcohol and/or drug services; crisis intervention (outpatient) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618106);

-- Alcohol and/or drug services; sub-acute detoxification (hospital inpatient) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618107);

-- Alcohol and/or drug services; acute detoxification (hospital inpatient) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618108);

-- Alcohol and/or drug services; sub-acute detoxification (residential addiction program inpatient) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618109);

-- Alcohol and/or drug services; acute detoxification (residential addiction program inpatient) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618110);

-- Alcohol and/or drug services; sub-acute detoxification (residential addiction program outpatient) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618111);

-- Alcohol and/or drug services; acute detoxification (residential addiction program outpatient) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618112);

-- Alcohol and/or drug services; ambulatory detoxification (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618113);

-- Alcohol and/or drug services (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618114);

-- Alcohol and/or drug services; medical/somatic (medical intervention in ambulatory setting) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618115);

-- Alcohol and/or drug services; methadone administration and/or service (provision of the drug by a licensed program) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618119);

-- Alcohol and/or drug intervention service (planned facilitation) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618121);

-- Behavioral health outreach service (planned approach to reach a targeted population) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618122);

-- Alcohol and/or drug prevention environmental service (broad range of external activities geared toward modifying systems in order to mainstream prevention through policy and law) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618126);

-- Alcohol and/or drug prevention problem identification and referral service (e.g., student assistance and employee assistance programs), does not include assessment (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618127);

-- Alcohol and/or other drug abuse services, not otherwise specified (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618146);

-- Alcohol and/or other drug testing: collection and handling only, specimens other than blood (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618147);

-- Alcohol and/or drug services, brief intervention, per 15 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618149);

-- Substance abuse program (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618194);

-- Opioid addiction treatment program (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618195);

-- Integrated mental health/substance abuse program (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618196);

-- Ambulatory setting substance abuse treatment or detoxification services, per diem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721545);

-- Alcohol and/or substance abuse services, family/couple counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721628);

-- Alcohol and/or substance abuse services, treatment plan development and/or modification (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721629);

-- Alcohol and/or substance abuse services, skills development (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721634);

-- Substance Abuse Treatment (Therapy) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801374);

-- Substance Abuse Treatment @ None (Therapy) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814217);

-- Substance Abuse Treatment, Detoxification Services (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795858);

-- Substance Abuse Treatment @ None @ Detoxification Services @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821591);

-- Substance Abuse Treatment @ None @ Detoxification Services @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894438);

-- Substance Abuse Treatment @ None @ Detoxification Services @ None @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814218);

-- Detoxification Services for Substance Abuse Treatment (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795859);

-- Substance Abuse Treatment, Individual Counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795860);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Cognitive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839393);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Cognitive @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834820);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Cognitive @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834821);

-- Individual Counseling for Substance Abuse Treatment, Cognitive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795861);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854454);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Behavioral @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854455);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Behavioral @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849572);

-- Individual Counseling for Substance Abuse Treatment, Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795862);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Cognitive-Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809351);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Cognitive-Behavioral @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2826848);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Cognitive-Behavioral @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862955);

-- Individual Counseling for Substance Abuse Treatment, Cognitive-Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795863);

-- Substance Abuse Treatment @ None @ Individual Counseling @ 12-Step (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862956);

-- Substance Abuse Treatment @ None @ Individual Counseling @ 12-Step @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875865);

-- Substance Abuse Treatment @ None @ Individual Counseling @ 12-Step @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849573);

-- Individual Counseling for Substance Abuse Treatment, 12-Step (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795864);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Interpersonal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849574);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Interpersonal @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889395);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Interpersonal @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2826849);

-- Individual Counseling for Substance Abuse Treatment, Interpersonal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795865);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Vocational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862957);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Vocational @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849575);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Vocational @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834822);

-- Individual Counseling for Substance Abuse Treatment, Vocational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795866);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Psychoeducation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875866);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Psychoeducation @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821592);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Psychoeducation @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821593);

-- Individual Counseling for Substance Abuse Treatment, Psychoeducation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795867);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Motivational Enhancement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821594);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Motivational Enhancement @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854456);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Motivational Enhancement @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894439);

-- Individual Counseling for Substance Abuse Treatment, Motivational Enhancement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795868);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Confrontational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854457);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Confrontational @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854458);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Confrontational @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2868120);

-- Individual Counseling for Substance Abuse Treatment, Confrontational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795869);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Continuing Care (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839394);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Continuing Care @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821595);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Continuing Care @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809352);

-- Individual Counseling for Substance Abuse Treatment, Continuing Care (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795870);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Spiritual (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839395);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Spiritual @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849576);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Spiritual @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839396);

-- Individual Counseling for Substance Abuse Treatment, Spiritual (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795871);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Pre/Post-Test Infectious Disease (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839397);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Pre/Post-Test Infectious Disease @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862958);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Pre/Post-Test Infectious Disease @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875992);

-- Individual Counseling for Substance Abuse Treatment, Pre/Post-Test Infectious Disease (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795872);

-- Substance Abuse Treatment, Group Counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795873);

-- Substance Abuse Treatment @ None @ Group Counseling @ Cognitive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894440);

-- Substance Abuse Treatment @ None @ Group Counseling @ Cognitive @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2868121);

-- Substance Abuse Treatment @ None @ Group Counseling @ Cognitive @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821596);

-- Group Counseling for Substance Abuse Treatment, Cognitive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795874);

-- Substance Abuse Treatment @ None @ Group Counseling @ Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875993);

-- Substance Abuse Treatment @ None @ Group Counseling @ Behavioral @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821597);

-- Substance Abuse Treatment @ None @ Group Counseling @ Behavioral @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834823);

-- Group Counseling for Substance Abuse Treatment, Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795875);

-- Substance Abuse Treatment @ None @ Group Counseling @ Cognitive-Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900289);

-- Substance Abuse Treatment @ None @ Group Counseling @ Cognitive-Behavioral @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814219);

-- Substance Abuse Treatment @ None @ Group Counseling @ Cognitive-Behavioral @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894441);

-- Group Counseling for Substance Abuse Treatment, Cognitive-Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795876);

-- Substance Abuse Treatment @ None @ Group Counseling @ 12-Step (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854459);

-- Substance Abuse Treatment @ None @ Group Counseling @ 12-Step @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894442);

-- Substance Abuse Treatment @ None @ Group Counseling @ 12-Step @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875994);

-- Group Counseling for Substance Abuse Treatment, 12-Step (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795877);

-- Substance Abuse Treatment @ None @ Group Counseling @ Interpersonal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849577);

-- Substance Abuse Treatment @ None @ Group Counseling @ Interpersonal @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839398);

-- Substance Abuse Treatment @ None @ Group Counseling @ Interpersonal @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894443);

-- Group Counseling for Substance Abuse Treatment, Interpersonal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795878);

-- Substance Abuse Treatment @ None @ Group Counseling @ Vocational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881411);

-- Substance Abuse Treatment @ None @ Group Counseling @ Vocational @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854460);

-- Substance Abuse Treatment @ None @ Group Counseling @ Vocational @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900290);

-- Group Counseling for Substance Abuse Treatment, Vocational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795879);

-- Substance Abuse Treatment @ None @ Group Counseling @ Psychoeducation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814220);

-- Substance Abuse Treatment @ None @ Group Counseling @ Psychoeducation @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875995);

-- Substance Abuse Treatment @ None @ Group Counseling @ Psychoeducation @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862959);

-- Group Counseling for Substance Abuse Treatment, Psychoeducation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795880);

-- Substance Abuse Treatment @ None @ Group Counseling @ Motivational Enhancement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839399);

-- Substance Abuse Treatment @ None @ Group Counseling @ Motivational Enhancement @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834824);

-- Substance Abuse Treatment @ None @ Group Counseling @ Motivational Enhancement @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839400);

-- Group Counseling for Substance Abuse Treatment, Motivational Enhancement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795881);

-- Substance Abuse Treatment @ None @ Group Counseling @ Confrontational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849578);

-- Substance Abuse Treatment @ None @ Group Counseling @ Confrontational @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849579);

-- Substance Abuse Treatment @ None @ Group Counseling @ Confrontational @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839401);

-- Group Counseling for Substance Abuse Treatment, Confrontational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795882);

-- Substance Abuse Treatment @ None @ Group Counseling @ Continuing Care (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881412);

-- Substance Abuse Treatment @ None @ Group Counseling @ Continuing Care @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849580);

-- Substance Abuse Treatment @ None @ Group Counseling @ Continuing Care @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894444);

-- Group Counseling for Substance Abuse Treatment, Continuing Care (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795883);

-- Substance Abuse Treatment @ None @ Group Counseling @ Spiritual (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875996);

-- Substance Abuse Treatment @ None @ Group Counseling @ Spiritual @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839402);

-- Substance Abuse Treatment @ None @ Group Counseling @ Spiritual @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900291);

-- Group Counseling for Substance Abuse Treatment, Spiritual (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795884);

-- Substance Abuse Treatment @ None @ Group Counseling @ Pre/Post-Test Infectious Disease (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2868122);

-- Substance Abuse Treatment @ None @ Group Counseling @ Pre/Post-Test Infectious Disease @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821598);

-- Substance Abuse Treatment @ None @ Group Counseling @ Pre/Post-Test Infectious Disease @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862960);

-- Group Counseling for Substance Abuse Treatment, Pre/Post-Test Infectious Disease (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795885);

-- Substance Abuse Treatment, Individual Psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795886);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Cognitive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839403);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Cognitive @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834825);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Cognitive @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875997);

-- Individual Psychotherapy for Substance Abuse Treatment, Cognitive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795887);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862961);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Behavioral @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900292);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Behavioral @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854461);

-- Individual Psychotherapy for Substance Abuse Treatment, Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795888);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Cognitive-Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889396);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Cognitive-Behavioral @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894445);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Cognitive-Behavioral @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875998);

-- Individual Psychotherapy for Substance Abuse Treatment, Cognitive-Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795889);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ 12-Step (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900293);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ 12-Step @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854462);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ 12-Step @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849581);

-- Individual Psychotherapy for Substance Abuse Treatment, 12-Step (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795890);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Interpersonal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854463);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Interpersonal @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862962);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Interpersonal @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881413);

-- Individual Psychotherapy for Substance Abuse Treatment, Interpersonal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795891);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Interactive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900294);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Interactive @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801375);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Interactive @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809353);

-- Individual Psychotherapy for Substance Abuse Treatment, Interactive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795892);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychoeducation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854464);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychoeducation @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889397);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychoeducation @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889398);

-- Individual Psychotherapy for Substance Abuse Treatment, Psychoeducation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795893);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Motivational Enhancement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821599);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Motivational Enhancement @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814221);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Motivational Enhancement @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881414);

-- Individual Psychotherapy for Substance Abuse Treatment, Motivational Enhancement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795894);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Confrontational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801376);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Confrontational @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875999);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Confrontational @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889399);

-- Individual Psychotherapy for Substance Abuse Treatment, Confrontational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795895);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Supportive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889400);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Supportive @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814222);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Supportive @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889401);

-- Individual Psychotherapy for Substance Abuse Treatment, Supportive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795896);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychoanalysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834826);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychoanalysis @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900295);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychoanalysis @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894446);

-- Individual Psychotherapy for Substance Abuse Treatment, Psychoanalysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795897);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychodynamic (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821600);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychodynamic @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849582);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychodynamic @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854465);

-- Individual Psychotherapy for Substance Abuse Treatment, Psychodynamic (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795898);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychophysiological (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801377);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychophysiological @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889402);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychophysiological @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2876000);

-- Individual Psychotherapy for Substance Abuse Treatment, Psychophysiological (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795899);

-- Substance Abuse Treatment, Family Counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795900);

-- Substance Abuse Treatment @ None @ Family Counseling @ Other Family Counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889403);

-- Substance Abuse Treatment @ None @ Family Counseling @ Other Family Counseling @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2826850);

-- Substance Abuse Treatment @ None @ Family Counseling @ Other Family Counseling @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809354);

-- Family Counseling for Substance Abuse Treatment (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795901);

-- Substance Abuse Treatment, Medication Management (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795902);

-- Substance Abuse Treatment @ None @ Medication Management @ Nicotine Replacement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889404);

-- Substance Abuse Treatment @ None @ Medication Management @ Nicotine Replacement @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2826851);

-- Substance Abuse Treatment @ None @ Medication Management @ Nicotine Replacement @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801378);

-- Medication Management for Substance Abuse Treatment, Nicotine Replacement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795903);

-- Substance Abuse Treatment @ None @ Medication Management @ Methadone Maintenance (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889405);

-- Substance Abuse Treatment @ None @ Medication Management @ Methadone Maintenance @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900296);

-- Substance Abuse Treatment @ None @ Medication Management @ Methadone Maintenance @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854466);

-- Medication Management for Substance Abuse Treatment, Methadone Maintenance (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796047);

-- Substance Abuse Treatment @ None @ Medication Management @ Levo-alpha-acetyl-methadol (LAAM) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862963);

-- Substance Abuse Treatment @ None @ Medication Management @ Levo-alpha-acetyl-methadol (LAAM) @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2876001);

-- Substance Abuse Treatment @ None @ Medication Management @ Levo-alpha-acetyl-methadol (LAAM) @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839404);

-- Medication Management for Substance Abuse Treatment, Levo-alpha-acetyl-methadol (LAAM) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796048);

-- Substance Abuse Treatment @ None @ Medication Management @ Antabuse (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889406);

-- Substance Abuse Treatment @ None @ Medication Management @ Antabuse @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834827);

-- Substance Abuse Treatment @ None @ Medication Management @ Antabuse @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862964);

-- Medication Management for Substance Abuse Treatment, Antabuse (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796049);

-- Substance Abuse Treatment @ None @ Medication Management @ Naltrexone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900297);

-- Substance Abuse Treatment @ None @ Medication Management @ Naltrexone @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809355);

-- Substance Abuse Treatment @ None @ Medication Management @ Naltrexone @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2826852);

-- Medication Management for Substance Abuse Treatment, Naltrexone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796050);

-- Substance Abuse Treatment @ None @ Medication Management @ Naloxone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814223);

-- Substance Abuse Treatment @ None @ Medication Management @ Naloxone @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881415);

-- Substance Abuse Treatment @ None @ Medication Management @ Naloxone @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849583);

-- Medication Management for Substance Abuse Treatment, Naloxone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796051);

-- Substance Abuse Treatment @ None @ Medication Management @ Clonidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881416);

-- Substance Abuse Treatment @ None @ Medication Management @ Clonidine @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854467);

-- Substance Abuse Treatment @ None @ Medication Management @ Clonidine @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801379);

-- Medication Management for Substance Abuse Treatment, Clonidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796052);

-- Substance Abuse Treatment @ None @ Medication Management @ Bupropion (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854468);

-- Substance Abuse Treatment @ None @ Medication Management @ Bupropion @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801380);

-- Substance Abuse Treatment @ None @ Medication Management @ Bupropion @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881417);

-- Medication Management for Substance Abuse Treatment, Bupropion (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796053);

-- Substance Abuse Treatment @ None @ Medication Management @ Psychiatric Medication (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849584);

-- Substance Abuse Treatment @ None @ Medication Management @ Psychiatric Medication @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814224);

-- Substance Abuse Treatment @ None @ Medication Management @ Psychiatric Medication @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814225);

-- Medication Management for Substance Abuse Treatment, Psychiatric Medication (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796054);

-- Substance Abuse Treatment @ None @ Medication Management @ Other Replacement Medication (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814226);

-- Substance Abuse Treatment @ None @ Medication Management @ Other Replacement Medication @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2876002);

-- Substance Abuse Treatment @ None @ Medication Management @ Other Replacement Medication @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821601);

-- Medication Management for Substance Abuse Treatment, Other Replacement Medication (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796055);

-- Substance Abuse Treatment, Pharmacotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796056);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Nicotine Replacement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881418);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Nicotine Replacement @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849585);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Nicotine Replacement @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839405);

-- Pharmacotherapy for Substance Abuse Treatment, Nicotine Replacement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796057);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Methadone Maintenance (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2876003);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Methadone Maintenance @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801512);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Methadone Maintenance @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900298);

-- Pharmacotherapy for Substance Abuse Treatment, Methadone Maintenance (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796058);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Levo-alpha-acetyl-methadol (LAAM) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900299);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Levo-alpha-acetyl-methadol (LAAM) @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862965);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Levo-alpha-acetyl-methadol (LAAM) @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849586);

-- Pharmacotherapy for Substance Abuse Treatment, Levo-alpha-acetyl-methadol (LAAM) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796059);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Antabuse (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821602);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Antabuse @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849587);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Antabuse @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834828);

-- Pharmacotherapy for Substance Abuse Treatment, Antabuse (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796060);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Naltrexone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814227);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Naltrexone @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881419);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Naltrexone @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881420);

-- Pharmacotherapy for Substance Abuse Treatment, Naltrexone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796061);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Naloxone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862966);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Naloxone @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854469);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Naloxone @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839406);

-- Pharmacotherapy for Substance Abuse Treatment, Naloxone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796062);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Clonidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849588);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Clonidine @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2826853);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Clonidine @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894447);

-- Pharmacotherapy for Substance Abuse Treatment, Clonidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796063);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Bupropion (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809356);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Bupropion @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2876004);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Bupropion @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849589);

-- Pharmacotherapy for Substance Abuse Treatment, Bupropion (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796064);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Psychiatric Medication (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814228);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Psychiatric Medication @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862967);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Psychiatric Medication @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2826854);

-- Pharmacotherapy for Substance Abuse Treatment, Psychiatric Medication (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796065);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Other Replacement Medication (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900300);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Other Replacement Medication @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889407);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Other Replacement Medication @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881421);

-- Pharmacotherapy for Substance Abuse Treatment, Other Replacement Medication (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796066);

-- Narcosynthesis for psychiatric diagnostic and therapeutic purposes (eg, sodium amobarbital (Amytal) interview) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213551);

-- Psychologic evaluation and testing (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007690);

-- Administration of intelligence test (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007691);

-- Developmental screening, with interpretation and report, per standardized instrument form (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314189);

-- Developmental testing, (includes assessment of motor, language, social, adaptive, and/or cognitive functioning by standardized developmental instruments) with interpretation and report (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314190);

-- Administration of psychologic test (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007702);

-- Character analysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007703);

-- Other psychologic evaluation and testing (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007704);

-- Psychologic mental status determination, not otherwise specified (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007705);

-- Psychiatric mental status determination (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007707);

-- Other psychiatric interview and evaluation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007710);

-- Routine psychiatric visit, not otherwise specified (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007708);

-- Psychiatric interviews, consultations, and evaluations (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007706);

-- Psychiatric commitment evaluation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007709);

-- Other counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007767);

-- Psychiatric diagnostic evaluation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43527984);

-- Psychiatric diagnostic evaluation with medical services (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43527985);

-- Psychiatric diagnostic interview examination (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213518);

-- Interactive psychiatric diagnostic interview examination using play equipment, physical devices, language interpreter, or other mechanisms of communication (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213519);

-- Psychological testing (includes psychodiagnostic assessment of personality, psychopathology, emotionality, intellectual abilities, eg, WAIS-R, Rorschach, MMPI) with interpretation and report, per hour (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42738926);

-- Psychological testing (includes psychodiagnostic assessment of emotionality, intellectual abilities, personality and psychopathology, eg, MMPI, Rorschach, WAIS), per hour of the psychologist's or physician's time, both face-to-face time administering test (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314185);

-- Psychological testing (includes psychodiagnostic assessment of emotionality, intellectual abilities, personality and psychopathology, eg, MMPI and WAIS), with qualified health care professional interpretation and report, administered by technician, per ho (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314186);

-- Psychological testing (includes psychodiagnostic assessment of emotionality, intellectual abilities, personality and psychopathology, eg, MMPI), administered by a computer, with qualified health care professional interpretation and report (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314187);

-- Neurobehavioral status exam (clinical assessment of thinking, reasoning and judgment, eg, acquired knowledge, attention, memory, visual spatial abilities, language functions, planning) with interpretation and report, per hour (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42738930);

-- Neurobehavioral status exam (clinical assessment of thinking, reasoning and judgment, eg, acquired knowledge, attention, language, memory, planning and problem solving, and visual spatial abilities), per hour of the psychologist's or physician's time, bot (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314192);

-- Neuropsychological testing battery (eg, Halstead-Reitan, Luria, WAIS-R) with interpretation and report, per hour (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42738931);

-- Neuropsychological testing (eg, Halstead-Reitan Neuropsychological Battery, Wechsler Memory Scales and Wisconsin Card Sorting Test), per hour of the psychologist's or physician's time, both face-to-face time administering tests to the patient and time int (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314194);

-- Neuropsychological testing (eg, Halstead-Reitan Neuropsychological Battery, Wechsler Memory Scales and Wisconsin Card Sorting Test), with qualified health care professional interpretation and report, administered by technician, per hour of technician time (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314195);

-- Neuropsychological testing (eg, Wisconsin Card Sorting Test), administered by a computer, with qualified health care professional interpretation and report (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314196);

-- Standardized cognitive performance testing (eg, Ross Information Processing Assessment) per hour of a qualified health care professional's time, both face-to-face time administering tests to the patient and time interpreting these test results and prepari (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314197);

-- Brief emotional/behavioral assessment (eg, depression inventory, attention-deficit/hyperactivity disorder [ADHD] scale), with scoring and documentation, per standardized instrument (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257498);

-- Health and behavior assessment (eg, health-focused clinical interview, behavioral observations, psychophysiological monitoring, health-oriented questionnaires), each 15 minutes face-to-face with the patient; initial assessment (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314198);

-- Health and behavior assessment (eg, health-focused clinical interview, behavioral observations, psychophysiological monitoring, health-oriented questionnaires), each 15 minutes face-to-face with the patient; re-assessment (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314199);

-- Psychiatric Diagnostic Procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45888161);

-- Health and behavior assessment (eg, health-focused clinical interview, behavioral observations, psychophysiological monitoring, health-oriented questionnaires), each 15 minutes face-to-face with the patient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45887787);

-- Patient screened for depression (SUD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2102214);

-- Negative screen for depressive symptoms as categorized by using a standardized depression screening/assessment tool (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107225);

-- No significant depressive symptoms as categorized by using a standardized depression assessment tool (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107229);

-- Mild to moderate depressive symptoms as categorized by using a standardized depression screening/assessment tool (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107245);

-- Clinically significant depressive symptoms as categorized by using a standardized depression screening/assessment tool (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107249);

-- Suicide risk assessed (MDD, MDD ADOL) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106303);

-- Brief emotional/behavioral assessment (eg, depression inventory, attention-deficit/hyperactivity disorder [ADHD] scale), with scoring and documentation, per standardized instrument (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257498);

-- PATIENT DOCUMENTED TO HAVE MENTAL STATUS ASSESSED (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617772);

-- Mental status assessed (CAP) (EM) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2102833);

-- One or more neuropsychiatric symptoms (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533178);

-- Mental health assessment, by non-physician (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618130);

-- Suicide risk assessed at the initial evaluation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533176);

-- Screening for clinical depression is documented as negative, a follow-up plan is not required (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617909);

-- Screening for clinical depression is documented as being positive and a follow-up plan is documented (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617831);

-- Screening for clinical depression documented as positive, follow-up plan not documented, reason not given (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617910);

-- Screening for clinical depression documented as positive, a follow-up plan not documented, documentation stating the patient is not eligible (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533193);

-- Assessment of depression severity at the initial evaluation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533277);

-- Annual depression screening, 15 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40664726);

-- Central Nervous System Assessments/Tests (eg, Neuro-Cognitive, Mental Status, Speech Testing) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45888918);

-- Psychiatric evaluation of hospital records, other psychiatric reports, psychometric and/or projective tests, and other accumulated data for medical diagnostic purposes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213558);

-- Mental Health, Psychological Tests (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795665);

-- Mental Health @ None @ Psychological Tests @ Developmental (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900283);

-- Mental Health @ None @ Psychological Tests @ Developmental @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900284);

-- Mental Health @ None @ Psychological Tests @ Developmental @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801370);

-- Psychological Tests, Developmental (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795666);

-- Mental Health @ None @ Psychological Tests @ Personality and Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814211);

-- Mental Health @ None @ Psychological Tests @ Personality and Behavioral @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839385);

-- Mental Health @ None @ Psychological Tests @ Personality and Behavioral @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834815);

-- Psychological Tests, Personality and Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795667);

-- Mental Health @ None @ Psychological Tests @ Intellectual and Psychoeducational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814212);

-- Mental Health @ None @ Psychological Tests @ Intellectual and Psychoeducational @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834816);

-- Mental Health @ None @ Psychological Tests @ Intellectual and Psychoeducational @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862946);

-- Psychological Tests, Intellectual and Psychoeducational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795668);

-- Mental Health @ None @ Psychological Tests @ Neuropsychological (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2867987);

-- Mental Health @ None @ Psychological Tests @ Neuropsychological @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839386);

-- Mental Health @ None @ Psychological Tests @ Neuropsychological @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862947);

-- Psychological Tests, Neuropsychological (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795669);

-- Mental Health @ None @ Psychological Tests @ Neurobehavioral and Cognitive Status (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862948);

-- Mental Health @ None @ Psychological Tests @ Neurobehavioral and Cognitive Status @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875858);

-- Mental Health @ None @ Psychological Tests @ Neurobehavioral and Cognitive Status @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814213);

-- Psychological Tests, Neurobehavioral and Cognitive Status (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795670);

-- all drugs of interest reflected in procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007723);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007724);

-- all drugs of interest reflected in procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108577);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257488);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257654);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257555);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212118);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44816350);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108573);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257739);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257410);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257697);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257553);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212119);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212122);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212106);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212111);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257696);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257552);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257738);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257591);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212113);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44816347);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44816361);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45889818);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212108);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212109);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44816351);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212121);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212114);

-- all drugs of interest reflected in procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45890740);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617571);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786399);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617574);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40659692);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40664583);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718424);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718425);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2720963);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718258);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786557);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718568);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718648);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718605);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718549);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786380);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718621);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721001);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718315);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718584);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718633);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2720945);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45890662);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45890663);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45890665);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40664472);

-- Subconvulsive electroshock therapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007727);

-- Other electroshock therapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007728);

-- Anesthesia for electroconvulsive therapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2100627);

-- Electroconvulsive therapy (includes necessary monitoring) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213552);

-- Electroconvulsive therapy (includes necessary monitoring); multiple seizures, per day (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42732301);

-- Electroconvulsive therapy (ECT) provided (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108578);

-- Mental Health, Electroconvulsive Therapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795842);

-- Mental Health @ None @ Electroconvulsive Therapy @ Unilateral-Single Seizure (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875862);

-- Mental Health @ None @ Electroconvulsive Therapy @ Unilateral-Single Seizure @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854452);

-- Mental Health @ None @ Electroconvulsive Therapy @ Unilateral-Single Seizure @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854453);

-- Electroconvulsive Therapy, Unilateral-Single Seizure (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795843);

-- Mental Health @ None @ Electroconvulsive Therapy @ Unilateral-Multiple Seizure (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881409);

-- Mental Health @ None @ Electroconvulsive Therapy @ Unilateral-Multiple Seizure @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809347);

-- Mental Health @ None @ Electroconvulsive Therapy @ Unilateral-Multiple Seizure @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839391);

-- Electroconvulsive Therapy, Unilateral-Multiple Seizure (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795844);

-- Mental Health @ None @ Electroconvulsive Therapy @ Bilateral-Single Seizure (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834818);

-- Mental Health @ None @ Electroconvulsive Therapy @ Bilateral-Single Seizure @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900287);

-- Mental Health @ None @ Electroconvulsive Therapy @ Bilateral-Single Seizure @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862952);

-- Electroconvulsive Therapy, Bilateral-Single Seizure (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795845);

-- Mental Health @ None @ Electroconvulsive Therapy @ Bilateral-Multiple Seizure (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839392);

-- Mental Health @ None @ Electroconvulsive Therapy @ Bilateral-Multiple Seizure @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875863);

-- Mental Health @ None @ Electroconvulsive Therapy @ Bilateral-Multiple Seizure @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862953);

-- Electroconvulsive Therapy, Bilateral-Multiple Seizure (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795846);

-- Mental Health @ None @ Electroconvulsive Therapy @ Other Electroconvulsive Therapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889393);

-- Mental Health @ None @ Electroconvulsive Therapy @ Other Electroconvulsive Therapy @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894436);

-- Mental Health @ None @ Electroconvulsive Therapy @ Other Electroconvulsive Therapy @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809348);

-- Other Electroconvulsive Therapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795847);

-- Mental Health, Light Therapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;light_therapy;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795856);

-- Mental Health @ None @ Light Therapy @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;light_therapy;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2868119);

-- Mental Health @ None @ Light Therapy @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;light_therapy;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894437);

-- Mental Health @ None @ Light Therapy @ None @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;light_therapy;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801373);

-- Light Therapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;light_therapy;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795857);

-- Mild to moderate depressive symptoms as categorized by using a standardized depression screening/assessment tool (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107245);

-- Clinically significant depressive symptoms as categorized by using a standardized depression screening/assessment tool (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107249);

-- Patient interviewed directly on or before date of diagnosis of major depressive disorder (MDD ADOL) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40757095);

-- Major depressive disorder, mild (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106304);

-- Major depressive disorder, moderate (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106305);

-- Major depressive disorder, severe without psychotic features (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106310);

-- Major depressive disorder, severe with psychotic features (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106322);

-- Major depressive disorder, in remission (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106324);

-- Documentation of new diagnosis of initial or recurrent episode of major depressive disorder (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106326);

-- Plan for follow-up care for major depressive disorder, documented (MDD ADOL) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40756912);

-- DSM-V criteria for major depressive disorder documented at the initial evaluation (MDD, MDD ADOL) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2101900);

-- Clinician documented that patient is not an eligible candidate for suicide risk assessment; major depressive disorder, in remission (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617865);

-- Documentation of new diagnosis of initial or recurrent episode of major depressive disorder (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617866);

-- Clinician treating major depressive disorder communicates to clinician treating comorbid condition (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533366);

-- Clinician treating major depressive disorder did not communicate to clinician treating comorbid condition, reason not given (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533229);

-- Clinician treating major depressive disorder did not communicate to clinician treating comorbid condition for specified patient reason (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786437);

-- Dsm-ivtm criteria for major depressive disorder documented at the initial evaluation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786417);

-- Dsm-iv-tr criteria for major depressive disorder not documented at the initial evaluation, reason not otherwise specified (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786418);

-- Performance measurement, evaluation of patient self assessment, depression (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721177);

-- Patient with a diagnosis of major depression documented as being treated with antidepressant medication during the entire 84 day (12 week) acute treatment phase (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617571);

-- Patient with a diagnosis of major depression not documented as being treated with antidepressant medication during the entire 84 day (12 week) acute treatment phase (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617572);

-- Patient with a diagnosis of major depression documented as being treated with antidepressant medication during the entire 180 day (6 month) continuation treatment phase (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786399);

-- Patient with a diagnosis of major depression not documented as being treated with antidepressant medication during the entire 180 day (6 months) continuation treatment phase (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786400);

-- Screening for clinical depression is documented as being positive and a follow-up plan is documented (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617831);

-- Screening for clinical depression documented as positive, follow-up plan not documented, reason not given (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617910);

-- Screening for clinical depression documented as positive, a follow-up plan not documented, documentation stating the patient is not eligible (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533193);

-- Clinician documented that patient with a diagnosis of major depression was not an eligible candidate for antidepressant medication treatment or patient did not have a diagnosis of major depression (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786398);

-- Patient documented as being treated with antidepressant medication for at least 6 months continuous treatment phase (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617574);

-- Individual psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007730);

-- Psychoanalysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007731);

-- Behavior therapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007743);

-- Individual therapy for psychosexual dysfunction (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007744);

-- Crisis intervention (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007745);

-- Play psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007746);

-- Exploratory verbal psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007747);

-- Supportive verbal psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007748);

-- Other individual psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007749);

-- Other psychotherapy and counselling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007750);

-- Group therapy for psychosexual dysfunction (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007751);

-- Family therapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007762);

-- Psychodrama (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007763);

-- Other group therapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007764);

-- Hypnotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007742);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an office or outpatient facility, approximately 20 to 30 minutes face-to-face with the patient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213520);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an office or outpatient facility, approximately 20 to 30 minutes face-to-face with the patient; with medical evaluation and management services (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213521);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an office or outpatient facility, approximately 45 to 50 minutes face-to-face with the patient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213522);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an office or outpatient facility, approximately 45 to 50 minutes face-to-face with the patient; with medical evaluation and management services (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213523);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an office or outpatient facility, approximately 75 to 80 minutes face-to-face with the patient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213524);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an office or outpatient facility, approximately 75 to 80 minutes face-to-face with the patient; with medical evaluation and management services (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213525);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an office or outpatient facility, approximately 20 to 30 minutes face-to-face with the patient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213526);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an office or outpatient facility, approximately 20 to 30 minutes face-to-face with the patient; with (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213527);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an office or outpatient facility, approximately 45 to 50 minutes face-to-face with the patient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213528);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an office or outpatient facility, approximately 45 to 50 minutes face-to-face with the patient; with (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213529);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an office or outpatient facility, approximately 75 to 80 minutes face-to-face with the patient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213530);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an office or outpatient facility, approximately 75 to 80 minutes face-to-face with the patient; with (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213531);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an inpatient hospital, partial hospital or residential care setting, approximately 20 to 30 minutes face-to-face with the patient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213532);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an inpatient hospital, partial hospital or residential care setting, approximately 20 to 30 minutes face-to-face with the patient; with medical evaluation and management (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213533);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an inpatient hospital, partial hospital or residential care setting, approximately 45 to 50 minutes face-to-face with the patient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213534);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an inpatient hospital, partial hospital or residential care setting, approximately 45 to 50 minutes face-to-face with the patient; with medical evaluation and management (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213535);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an inpatient hospital, partial hospital or residential care setting, approximately 75 to 80 minutes face-to-face with the patient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213536);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an inpatient hospital, partial hospital or residential care setting, approximately 75 to 80 minutes face-to-face with the patient; with medical evaluation and management (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213537);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an inpatient hospital, partial hospital or residential care setting, approximately 20 to 30 minutes f (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213538);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an inpatient hospital, partial hospital or residential care setting, approximately 20 to 30 minutes f (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213539);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an inpatient hospital, partial hospital or residential care setting, approximately 45 to 50 minutes f (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213540);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an inpatient hospital, partial hospital or residential care setting, approximately 45 to 50 minutes f (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213541);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an inpatient hospital, partial hospital or residential care setting, approximately 75 to 80 minutes f (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213542);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an inpatient hospital, partial hospital or residential care setting, approximately 75 to 80 minutes f (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213543);

-- Psychotherapy, 30 minutes with patient and/or family member (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43527986);

-- Psychotherapy, 30 minutes with patient and/or family member when performed with an evaluation and management service (List separately in addition to the code for primary procedure) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43527987);

-- Psychotherapy, 45 minutes with patient and/or family member (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43527904);

-- Psychotherapy, 45 minutes with patient and/or family member when performed with an evaluation and management service (List separately in addition to the code for primary procedure) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43527988);

-- Psychotherapy, 60 minutes with patient and/or family member (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43527905);

-- Psychotherapy, 60 minutes with patient and/or family member when performed with an evaluation and management service (List separately in addition to the code for primary procedure) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43527989);

-- Psychotherapy for crisis; first 60 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43527990);

-- Psychotherapy for crisis; each additional 30 minutes (List separately in addition to code for primary service) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43527991);

-- Psychoanalysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213544);

-- Family psychotherapy (without the patient present) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213545);

-- Family psychotherapy (conjoint psychotherapy) (with patient present) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213546);

-- Multiple-family group psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213547);

-- Group psychotherapy (other than of a multiple-family group) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213548);

-- Interactive group psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213549);

-- Individual psychophysiological therapy incorporating biofeedback training by any modality (face-to-face with the patient), with psychotherapy (eg, insight oriented, behavior modifying or supportive psychotherapy); 30 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213554);

-- Individual psychophysiological therapy incorporating biofeedback training by any modality (face-to-face with the patient), with psychotherapy (eg, insight oriented, behavior modifying or supportive psychotherapy); 45 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213555);

-- Other Psychotherapy Procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45887728);

-- Individual psychophysiological therapy incorporating biofeedback training by any modality (face-to-face with the patient), with psychotherapy (eg, insight oriented, behavior modifying or supportive psychotherapy) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45890023);

-- Interactive Complexity Psychiatry Services and Procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45889017);

-- Psychotherapy Services and Procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45887951);

-- Interpretation or explanation of results of psychiatric, other medical examinations and procedures, or other accumulated data to family or other responsible persons, or advising them how to assist patient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213559);

-- Psychotherapy for Crisis Services and Procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45888237);

-- Psychotherapy for crisis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45889353);

-- Psychotherapy services provided (MDD, MDD ADOL) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108571);

-- Health and behavior intervention, each 15 minutes, face-to-face; individual (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314200);

-- Health and behavior intervention, each 15 minutes, face-to-face; group (2 or more patients) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314201);

-- Health and behavior intervention, each 15 minutes, face-to-face; family (with the patient present) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314202);

-- Health and behavior intervention, each 15 minutes, face-to-face; family (without the patient present) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314203);

-- Pharmacologic management, including prescription and review of medication, when performed with psychotherapy services (List separately in addition to the code for primary procedure) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43527992);

-- Hypnotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213556);

-- Environmental intervention for medical management purposes on a psychiatric patient's behalf with agencies, employers, or institutions (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213557);

-- Group psychotherapy other than of a multiple-family group, in a partial hospitalization setting, approximately 45 to 50 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617477);

-- Interactive group psychotherapy, in a partial hospitalization setting, approximately 45 to 50 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617478);

-- Behavioral health counseling and therapy, per 15 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618103);

-- Community psychiatric supportive treatment, face-to-face, per 15 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618135);

-- Community psychiatric supportive treatment program, per diem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618136);

-- Behavioral health day treatment, per hour (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618162);

-- Family stabilization services, per 15 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721548);

-- Crisis intervention mental health services, per hour (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721549);

-- Crisis intervention mental health services, per diem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721550);

-- Activity therapy, such as music, dance, art or play therapies not for recreation, related to the care and treatment of patient's disabling mental health problems, per session (45 minutes or more) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617264);

-- Social work and psychological services, directly relating to and/or furthering the patient's rehabilitation goals, each 15 minutes, face-to-face; individual (services provided by a corf-qualified social worker or psychologist in a corf) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617476);

-- Psychosocial rehabilitation services, per 15 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618167);

-- Psychosocial rehabilitation services, per diem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618168);

-- Mental health clubhouse services, per 15 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618180);

-- Mental health clubhouse services, per diem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618181);

-- Training and educational services related to the care and treatment of patient's disabling mental health problems per session (45 minutes or more) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617265);

-- Psychoeducational service, per 15 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618177);

-- Mental Health, Crisis Intervention (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795671);

-- Mental Health @ None @ Crisis Intervention @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854450);

-- Mental Health @ None @ Crisis Intervention @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839387);

-- Mental Health @ None @ Crisis Intervention @ None @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801371);

-- Crisis Intervention (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795672);

-- Mental Health, Individual Psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795675);

-- Mental Health @ None @ Individual Psychotherapy @ Interactive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894432);

-- Mental Health @ None @ Individual Psychotherapy @ Interactive @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809343);

-- Mental Health @ None @ Individual Psychotherapy @ Interactive @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889389);

-- Individual Psychotherapy, Interactive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795676);

-- Mental Health @ None @ Individual Psychotherapy @ Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814214);

-- Mental Health @ None @ Individual Psychotherapy @ Behavioral @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894433);

-- Mental Health @ None @ Individual Psychotherapy @ Behavioral @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849566);

-- Individual Psychotherapy, Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795677);

-- Mental Health @ None @ Individual Psychotherapy @ Cognitive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894434);

-- Mental Health @ None @ Individual Psychotherapy @ Cognitive @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889390);

-- Mental Health @ None @ Individual Psychotherapy @ Cognitive @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889391);

-- Individual Psychotherapy, Cognitive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795678);

-- Mental Health @ None @ Individual Psychotherapy @ Interpersonal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814215);

-- Mental Health @ None @ Individual Psychotherapy @ Interpersonal @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2867988);

-- Mental Health @ None @ Individual Psychotherapy @ Interpersonal @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849567);

-- Individual Psychotherapy, Interpersonal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795679);

-- Mental Health @ None @ Individual Psychotherapy @ Psychoanalysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839388);

-- Mental Health @ None @ Individual Psychotherapy @ Psychoanalysis @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900285);

-- Mental Health @ None @ Individual Psychotherapy @ Psychoanalysis @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875859);

-- Individual Psychotherapy, Psychoanalysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795680);

-- Mental Health @ None @ Individual Psychotherapy @ Psychodynamic (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849568);

-- Mental Health @ None @ Individual Psychotherapy @ Psychodynamic @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881407);

-- Mental Health @ None @ Individual Psychotherapy @ Psychodynamic @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854451);

-- Individual Psychotherapy, Psychodynamic (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795681);

-- Mental Health @ None @ Individual Psychotherapy @ Supportive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809344);

-- Mental Health @ None @ Individual Psychotherapy @ Supportive @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834817);

-- Mental Health @ None @ Individual Psychotherapy @ Supportive @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862949);

-- Individual Psychotherapy, Supportive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795682);

-- Mental Health @ None @ Individual Psychotherapy @ Cognitive-Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801372);

-- Mental Health @ None @ Individual Psychotherapy @ Cognitive-Behavioral @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2868118);

-- Mental Health @ None @ Individual Psychotherapy @ Cognitive-Behavioral @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2826845);

-- Individual Psychotherapy, Cognitive-Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795683);

-- Mental Health @ None @ Individual Psychotherapy @ Psychophysiological (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862950);

-- Mental Health @ None @ Individual Psychotherapy @ Psychophysiological @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821590);

-- Mental Health @ None @ Individual Psychotherapy @ Psychophysiological @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839389);

-- Individual Psychotherapy, Psychophysiological (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795684);

-- Mental Health, Counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795836);

-- Mental Health @ None @ Counseling @ Educational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839390);

-- Mental Health @ None @ Counseling @ Educational @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809345);

-- Mental Health @ None @ Counseling @ Educational @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889392);

-- Educational Counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795837);

-- Mental Health @ None @ Counseling @ Vocational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900286);

-- Mental Health @ None @ Counseling @ Vocational @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875860);

-- Mental Health @ None @ Counseling @ Vocational @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809346);

-- Vocational Counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795838);

-- Mental Health @ None @ Counseling @ Other Counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2826846);

-- Mental Health @ None @ Counseling @ Other Counseling @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875861);

-- Mental Health @ None @ Counseling @ Other Counseling @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894435);

-- Other Counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795839);

-- Mental Health, Family Psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795840);

-- Mental Health @ None @ Family Psychotherapy @ Other Family Psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849569);

-- Mental Health @ None @ Family Psychotherapy @ Other Family Psychotherapy @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881408);

-- Mental Health @ None @ Family Psychotherapy @ Other Family Psychotherapy @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862951);

-- Family Psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795841);

-- Mental Health, Biofeedback (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795848);

-- Mental Health @ None @ Biofeedback @ Other Biofeedback (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881410);

-- Mental Health @ None @ Biofeedback @ Other Biofeedback @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849570);

-- Mental Health @ None @ Biofeedback @ Other Biofeedback @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862954);

-- Biofeedback (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795849);

-- Mental Health, Hypnosis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795850);

-- Mental Health @ None @ Hypnosis @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814216);

-- Mental Health @ None @ Hypnosis @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2826847);

-- Mental Health @ None @ Hypnosis @ None @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900288);

-- Hypnosis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795851);

-- Mental Health, Narcosynthesis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795852);

-- Mental Health @ None @ Narcosynthesis @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849571);

-- Mental Health @ None @ Narcosynthesis @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809349);

-- Mental Health @ None @ Narcosynthesis @ None @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834819);

-- Narcosynthesis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795853);

-- Mental Health, Group Psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795854);

-- Mental Health @ None @ Group Psychotherapy @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889394);

-- Mental Health @ None @ Group Psychotherapy @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875864);

-- Mental Health @ None @ Group Psychotherapy @ None @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809350);

-- Group Psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795855);

-- Referral for psychologic rehabilitation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;referral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007768);

-- Referral for psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;referral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007769);

-- Referral for psychiatric aftercare (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;referral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007770);

-- Referral for vocational rehabilitation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;referral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007783);

-- Referral for other psychologic rehabilitation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;referral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007784);

-- Patient referral for psychotherapy documented (MDD, MDD ADOL) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;referral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108572);

-- Polysomnography; younger than 6 years, sleep staging with 4 or more additional parameters of sleep, attended by a technologist (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43528014);

-- Polysomnography; younger than 6 years, sleep staging with 4 or more additional parameters of sleep, with initiation of continuous positive airway pressure therapy or bi-level ventilation, attended by a technologist (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43528015);

-- Sleep study, unattended, simultaneous recording; heart rate, oxygen saturation, respiratory analysis (eg, by airflow or peripheral arterial tone), and sleep time (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40757072);

-- Sleep study, unattended, simultaneous recording; minimum of heart rate, oxygen saturation, and respiratory analysis (eg, by airflow or peripheral arterial tone) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40756854);

-- Multiple sleep latency or maintenance of wakefulness testing, recording, analysis and interpretation of physiological measurements of sleep during multiple trials to assess sleepiness (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314095);

-- Sleep study, unattended, simultaneous recording of, heart rate, oxygen saturation, respiratory airflow, and respiratory effort (eg, thoracoabdominal movement) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314096);

-- Sleep study, simultaneous recording of ventilation, respiratory effort, ECG or heart rate, and oxygen saturation, attended by a technologist (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314097);

-- Polysomnography; any age, sleep staging with 1-3 additional parameters of sleep, attended by a technologist (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314098);

-- Polysomnography; age 6 years or older, sleep staging with 4 or more additional parameters of sleep, attended by a technologist (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314099);

-- Polysomnography; age 6 years or older, sleep staging with 4 or more additional parameters of sleep, with initiation of continuous positive airway pressure therapy or bilevel ventilation, attended by a technologist (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314100);

-- Home visit for polysomnography and sleep studies (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42739877);

-- Sleep Medicine Testing Procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45888731);

-- Polysomnography (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45890139);

-- Sleep study, unattended, simultaneous recording (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45888226);

-- Sleep study, unattended, simultaneous recording; heart rate, oxygen saturation, respiratory analysis (eg, by airflow or peripheral arterial tone) and sleep time (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42741857);

-- Sleep study, unattended, simultaneous recording; minimum of heart rate, oxygen saturation, and respiratory analysis (eg, by airflow or peripheral arterial tone) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42741849);

-- Continuous measurement of wheeze rate during treatment assessment or during sleep for documentation of nocturnal wheeze and cough for diagnostic evaluation 3 to 24 hours, with interpretation and report (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40756984);

-- Home sleep study test (hst) with type ii portable monitor, unattended; minimum of 7 channels: eeg, eog, emg, ecg/heart rate, airflow, respiratory effort and oxygen saturation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617466);

-- Home sleep test (hst) with type iii portable monitor, unattended; minimum of 4 channels: 2 respiratory movement/airflow, 1 ecg/heart rate and 1 oxygen saturation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617467);

-- Home sleep test (hst) with type iv portable monitor, unattended; minimum of 3 channels (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617468);

-- Suicide risk assessed (MDD, MDD ADOL) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106303);

-- Negative screen for depressive symptoms as categorized by using a standardized depression screening/assessment tool (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107225);

-- No significant depressive symptoms as categorized by using a standardized depression assessment tool (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107229);

-- Mild to moderate depressive symptoms as categorized by using a standardized depression screening/assessment tool (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107245);

-- Clinically significant depressive symptoms as categorized by using a standardized depression screening/assessment tool (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107249);

-- Brief emotional/behavioral assessment (eg, depression inventory, attention-deficit/hyperactivity disorder [ADHD] scale), with scoring and documentation, per standardized instrument (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257498);

-- Patient screened for depression (SUD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2102214);

-- Suicide risk assessed at the initial evaluation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533176);

-- Annual depression screening, 15 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40664726);

-- Screening for clinical depression is documented as negative, a follow-up plan is not required (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617909);

-- Screening for clinical depression is documented as being positive and a follow-up plan is documented (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617831);

-- Screening for clinical depression documented as positive, follow-up plan not documented, reason not given (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617910);

-- Screening for clinical depression documented as positive, a follow-up plan not documented, documentation stating the patient is not eligible (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533193);

-- Assessment of depression severity at the initial evaluation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533277);

-- Therapeutic repetitive transcranial magnetic stimulation (TMS) treatment; initial, including cortical mapping, motor threshold determination, delivery and management (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;transcranial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40757109);

-- Therapeutic repetitive transcranial magnetic stimulation (TMS) treatment; subsequent delivery and management, per session (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;transcranial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40756837);

-- Therapeutic repetitive transcranial magnetic stimulation (TMS) treatment; subsequent motor threshold re-determination with delivery and management (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;transcranial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42742381);

-- Therapeutic repetitive transcranial magnetic stimulation (TMS) treatment (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;transcranial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45888229);

-- Therapeutic repetitive transcranial magnetic stimulation treatment delivery and management, per session (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'procedures;mental_procedure;transcranial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42741151);
