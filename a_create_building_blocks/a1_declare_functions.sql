-- STEP 1 OF 5: Declaration of user-defined functions used by CODEX

------------------------------------------------------ temporal logic functions

-- codex_instant: Create a daterange object from one date
-- notes: - the upper bound of the range is excluded,
--          ('[)') and set to the date plus one day
DROP FUNCTION IF EXISTS {{work_schema}}.codex_instant (DATE);
CREATE FUNCTION {{work_schema}}.codex_instant
  (instant_date DATE)
  RETURNS DATERANGE AS
  $$
    SELECT daterange(instant_date, instant_date + 1)
  $$
  LANGUAGE SQL IMMUTABLE;

-- codex_is_period: Test if a daterange object is an instant
DROP FUNCTION IF EXISTS {{work_schema}}.codex_is_instant (DATERANGE);
CREATE FUNCTION {{work_schema}}.codex_is_instant
  (instant_or_period DATERANGE)
  RETURNS BOOLEAN AS
  $$
    SELECT (
            CASE upper(instant_or_period) -
                 lower(instant_or_period)
              WHEN 1 THEN true
              ELSE false
            END
           );
  $$
  LANGUAGE SQL IMMUTABLE;

-- codex_period: Create a daterange object from two dates
-- notes: - the upper bound of the range is excluded,
--          and set to the end date plus one day
DROP FUNCTION IF EXISTS {{work_schema}}.codex_period (DATE, DATE);
CREATE FUNCTION {{work_schema}}.codex_period
  (start_date DATE,
   end_date DATE)
  RETURNS DATERANGE AS
  $$
    SELECT daterange(start_date, coalesce(end_date, start_date) + 1)
  $$
  LANGUAGE SQL IMMUTABLE;

-- codex_is_period: Test if a daterange object is a period
DROP FUNCTION IF EXISTS {{work_schema}}.codex_is_period (DATERANGE);
CREATE FUNCTION {{work_schema}}.codex_is_period
  (instant_or_period DATERANGE)
  RETURNS BOOLEAN AS
  $$
    SELECT NOT {{work_schema}}.codex_is_instant(instant_or_period);
  $$
  LANGUAGE SQL IMMUTABLE;

-- codex_period_contains: Check if a date is contained in an array of periods
-- example:
--   <- SELECT codex_period_contains(
--        '2008-05-15'::date,
--        '{
--        "[2008-04-01,2008-04-30)",
--        "[2008-05-01,2008-05-30)",
--        "[2008-06-01,2008-06-30)"
--        }'::DATERANGE[]);
--   -> t
-- notes: adapted from https://github.com/worden341/pgchronos
DROP FUNCTION IF EXISTS {{work_schema}}.codex_period_contains (DATE, DATERANGE[]);
CREATE FUNCTION {{work_schema}}.codex_period_contains
  (query_date DATE,
   periods DATERANGE[])
  RETURNS BOOLEAN AS
  $$
    SELECT coalesce(
             (SELECT true
                FROM (
                      SELECT unnest(periods) AS period
                     ) AS period
               WHERE (query_date <@ period.period)
               LIMIT 1
             ),
             false);
  $$
  LANGUAGE SQL IMMUTABLE;

DROP FUNCTION IF EXISTS {{work_schema}}.codex_period_contains (DATERANGE[], DATE);
CREATE FUNCTION {{work_schema}}.codex_period_contains
  (periods DATERANGE[],
   query_date DATE)
  RETURNS BOOLEAN AS
  $$
    SELECT {{work_schema}}.codex_period_contains(query_date, periods);
  $$
  LANGUAGE SQL IMMUTABLE;

-- codex_period_union: Return the union of a set of periods
-- example:
--   <- SELECT codex_period_union('{
--        "[2008-03-01,2008-03-02)",
--        "[2008-03-02,2008-04-01)",
--        "[2008-04-02,2008-05-01)",
--        "[2008-04-02,2018-05-01)"
--      }'::DATERANGE[]);
--   -> {"[2008-03-01,2008-04-01)","[2008-04-02,2018-05-01)"}
-- notes: adapted from https://github.com/worden341/pgchronos
DROP FUNCTION IF EXISTS {{work_schema}}.codex_period_union (DATERANGE[]);
CREATE FUNCTION {{work_schema}}.codex_period_union
  (periods DATERANGE[])
  RETURNS DATERANGE[] AS
  $$
    SELECT array_agg(merged_periods)
      FROM (
              SELECT daterange(start_date, min(end_date)) AS merged_periods
                FROM ( -- list period lower bounds that are not overlapping
                       -- with any other period in the input set
                      SELECT DISTINCT lower(period) AS start_date
                        FROM unnest(periods) AS period
                       WHERE (NOT {{work_schema}}.codex_period_contains(
                               lower(period) - 1, periods))
                     ) AS t_in
                JOIN ( -- list period upper bounds that are not overlapping
                       -- with any other period in the input set
                      SELECT upper(period) AS end_date
                        FROM unnest(periods) AS period
                       WHERE (NOT {{work_schema}}.codex_period_contains(
                               upper(period), periods))
                     ) AS t_out
                     -- get the cartesian product of these lower and upper
                     -- bounds, with the constraint that lower < upper
                  ON (t_in.start_date < t_out.end_date)
            GROUP BY t_in.start_date
            ORDER BY t_in.start_date
           ) AS results;
  $$
  LANGUAGE SQL IMMUTABLE;

-- codex_period_length: Return the length of a period (or instant)
-- example:
--   <- SELECT codex_period_length(
--        '[2008-03-02,2008-04-01)'::DATERANGE');
--   -> 30
DROP FUNCTION IF EXISTS {{work_schema}}.codex_period_length (DATERANGE);
CREATE FUNCTION {{work_schema}}.codex_period_length
  (period DATERANGE)
  RETURNS INTEGER AS
  $$
    SELECT coalesce(upper(period) - lower(period), 0);
  $$
  LANGUAGE SQL IMMUTABLE;

-- codex_period_length: Return the summed length of a set of periods
-- (or instants), not performing any union of the periods
-- example:
--   <- SELECT codex_period_length('{
--        "[2008-03-01,2008-03-02)",
--        "[2008-03-02,2008-04-01)"
--      }'::DATERANGE[]);
--   -> 31
DROP FUNCTION IF EXISTS {{work_schema}}.codex_period_length (DATERANGE[]);
CREATE FUNCTION {{work_schema}}.codex_period_length
  (periods DATERANGE[])
  RETURNS INTEGER AS
  $$
    SELECT sum(length)::INTEGER
      FROM (
            SELECT coalesce(upper(period) - lower(period), 0) AS length
              FROM unnest(periods) AS period
           ) AS lengths;
  $$
  LANGUAGE SQL IMMUTABLE;

-------------------------------------------------- vocabulary-related functions

-- codex_concept_descendants: Return a subset of the concept table with
-- all the concepts descending from a query concept (including itself);
-- the descendants are sorted by increasing distance from the concept
-- example:
--   <- SELECT concept_id, concept_name
--        FROM codex_concept_descendants(436665);
--   -> +------------+-----------------------------------------------------+
--      | concept_id | concept_name                                        |
--      +------------+-----------------------------------------------------+
--      |    4307956 | Bipolar II                                          |
--      |    4200385 | Severe bipolar disorder without psychotic features  |
--      |    4220617 | Severe bipolar I disorder, single manic episode ... |
--      |     432290 | Mild bipolar I disorder, single manic episode       |
--      |     439248 | Mixed bipolar affective disorder, moderate          |
--       ...
DROP FUNCTION IF EXISTS {{work_schema}}.codex_concept_descendants (INTEGER);
CREATE FUNCTION {{work_schema}}.codex_concept_descendants
  (concept_id INTEGER)
  RETURNS SETOF {{cdm_schema}}.concept AS
  $$
      SELECT concept.*
        FROM {{cdm_schema}}.concept,
             {{cdm_schema}}.concept_ancestor
       WHERE (concept_ancestor.ancestor_concept_id = $1)
         AND (concept.concept_id = concept_ancestor.descendant_concept_id)
    ORDER BY concept_ancestor.min_levels_of_separation;
  $$
  LANGUAGE SQL STABLE;

-- codex_concept_descendants: Return a subset of the concept table with
-- all the concepts descending from a list of query concepts (including these);
-- the descendants are sorted by increasing distance from the query concepts
-- example:
--   <- SELECT * FROM codex_concept_descendants(array[1234, 5678]);
DROP FUNCTION IF EXISTS {{work_schema}}.codex_concept_descendants (INTEGER[]);
CREATE FUNCTION {{work_schema}}.codex_concept_descendants
  (concept_id INTEGER[])
  RETURNS SETOF {{cdm_schema}}.concept AS
  $$
      SELECT concept.*
        FROM {{cdm_schema}}.concept,
             {{cdm_schema}}.concept_ancestor
       WHERE (concept_ancestor.ancestor_concept_id IN (
              SELECT unnest($1) AS concept_ids))
         AND (concept.concept_id = concept_ancestor.descendant_concept_id)
    ORDER BY concept_ancestor.min_levels_of_separation;
  $$
  LANGUAGE SQL STABLE;

-- codex_concept_ancestors: Return a subset of the concept table with
-- all the concepts leading to a query concept (including itself);
-- the ancestors are sorted by increasing distance from the concept
-- example:
--   <- SELECT concept_id, concept_name
--        FROM codex_concept_ancestors(436665);
--   -> +------------+------------------+
--      | concept_id |   concept_name   |
--      +------------+------------------+
--      |     432586 | Mental disorder  |
--      |     436665 | Bipolar disorder |
--      |     441840 | Clinical finding |
--      |     444100 | Mood disorder    |
--      |    4274025 | Disease          |
--      +------------+------------------+
DROP FUNCTION IF EXISTS {{work_schema}}.codex_concept_ancestors (INTEGER);
CREATE FUNCTION {{work_schema}}.codex_concept_ancestors
  (concept_id INTEGER)
  RETURNS SETOF {{cdm_schema}}.concept AS
  $$
      SELECT concept.*
        FROM {{cdm_schema}}.concept,
             {{cdm_schema}}.concept_ancestor
       WHERE (concept_ancestor.descendant_concept_id = $1)
         AND (concept.concept_id = concept_ancestor.ancestor_concept_id)
    ORDER BY concept_ancestor.min_levels_of_separation;
  $$
  LANGUAGE SQL STABLE;

-- codex_concept_ancestors: Return a subset of the concept table with
-- all the concepts leading to a list of query concepts (including these);
-- the ancestors are sorted by increasing distance from the query concepts
-- example:
--   <- SELECT * FROM codex_concept_ancestors(array[1234, 5678]);
DROP FUNCTION IF EXISTS {{work_schema}}.codex_concept_ancestors (INTEGER[]);
CREATE FUNCTION {{work_schema}}.codex_concept_ancestors
  (concept_id INTEGER[])
  RETURNS SETOF {{cdm_schema}}.concept AS
  $$
      SELECT concept.*
        FROM {{cdm_schema}}.concept,
             {{cdm_schema}}.concept_ancestor
       WHERE (concept_ancestor.descendant_concept_id IN (
              SELECT unnest($1) AS concept_id))
         AND (concept.concept_id = concept_ancestor.ancestor_concept_id)
    ORDER BY concept_ancestor.min_levels_of_separation;
  $$
  LANGUAGE SQL STABLE;

-------------------------------------------------- array manipulation functions

-- Return the minimum value is an array of integers
DROP FUNCTION IF EXISTS array_min (BIGINT[]);
CREATE FUNCTION array_min
  (BIGINT[])
  RETURNS BIGINT AS
  $$
    SELECT min(_value)
      FROM unnest($1) AS _value
  $$
  LANGUAGE SQL IMMUTABLE;

-- Aggregates arrays by concatenating them
DROP AGGREGATE IF EXISTS array_cat_agg (ANYARRAY);
CREATE AGGREGATE array_cat_agg (ANYARRAY) (
  SFUNC = array_cat, STYPE = ANYARRAY);

-- Filter out duplicates from a given array
DROP FUNCTION IF EXISTS array_distinct (ANYARRAY);
CREATE FUNCTION array_distinct
  (ANYARRAY)
  RETURNS ANYARRAY AS
  $$
    SELECT array_agg(DISTINCT _value)
      FROM unnest($1) AS _array(_value);
  $$
  LANGUAGE SQL IMMUTABLE;

-- Return elements in the first array that
-- are not found in the second array
DROP FUNCTION IF EXISTS array_difference (ANYARRAY, ANYARRAY);
CREATE FUNCTION array_difference
  (ANYARRAY, ANYARRAY)
  RETURNS ANYARRAY AS
  $$
    SELECT array(
             SELECT unnest($1)
             EXCEPT
             SELECT unnest($2));
  $$
  LANGUAGE SQL IMMUTABLE;

-- Sort elements of an array
DROP FUNCTION IF EXISTS array_sort (ANYARRAY);
CREATE FUNCTION array_sort
  (ANYARRAY)
  RETURNS ANYARRAY AS
  $$
    SELECT ARRAY(SELECT unnest($1) ORDER BY 1)
  $$
  LANGUAGE SQL IMMUTABLE;

-- Compare an array of strings (queries) against another
-- array of strings (patterns), returning TRUE if at least
-- one of the queries match one of the patterns
DROP FUNCTION IF EXISTS array_like (TEXT[], TEXT[]);
CREATE FUNCTION array_like
  (queries TEXT[],
   patterns TEXT[])
  RETURNS BOOLEAN AS
  $$
    SELECT (EXISTS (
              SELECT TRUE
                FROM unnest(queries) AS array_a,
             LATERAL unnest(patterns) AS array_b
               WHERE (array_a LIKE array_b)
           ));
  $$
  LANGUAGE SQL IMMUTABLE;
